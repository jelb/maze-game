import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;

import enums.Choice;
import pages.GameMode;
import pages.LoadPage;
import pages.MainMenu;

/*
 * Controls:
 * - arrow keys  	move
 * - WASD			move
 * - 1, 2, 3 		toggle max moveSpeed
 * - space			toggle "butter"
 * - h				hint
 * 
 * toggles probably going to be changed to powerups
 * maybe butter can be used for an extension game mode where the walls kill you
 */
/**
 * MazeGame
 * 
 * holds main function 1. title page (simple page that will have 2 buttons for
 * now, Play and Quit 2. Play: generates a maze, adds a player, stops when the
 * player wins
 * 
 */
public class MazeGame {

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		// dark blueish background colour
		frame.getContentPane().setBackground(new Color(20, 50, 100));
		frame.pack();
		frame.setPreferredSize(new Dimension(1200 + frame.getInsets().left
				+ frame.getInsets().right, 900 + frame.getInsets().bottom
				+ frame.getInsets().top));
		frame.pack();
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		while (true) {
			frame.getContentPane().setBackground(new Color(20, 50, 100));
			Choice choice = MainMenu.show(frame);
			if (choice == Choice.PLAY) {
				GameMode.show(frame);
			} else if (choice == Choice.LOAD) {
				LoadPage.show(frame);
			} else if (choice == Choice.QUIT) {
				System.exit(1);
			}
		}
	}
}
