package slideNinjaSlide;

import enums.SNSDifficulty;
import gameObjects.Cheese;
import gameObjects.EnemyHorde;
import gameObjects.GamePanel;
import gameObjects.Player;
import gameObjects.SNSMap;
import interfaceObjects.SNSGui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;

import pages.SNSDefeat;
import pages.SNSVictory;
import playerMovement.SNSMovement;

/**
 * This is where the Slide Ninja Slide game is run from. It listens for and
 * determines player movement.
 * 
 * @author Jeffrey
 * 
 */
public class PlaySlideNinjaSlide {
	private static boolean playing;
	// variables for thrust system
	private static int cooldownLength;
	private static int[] cooldownElapsed;
	private static boolean[] ready;
	// fields for timer
	private static Timer gameTimer;
	private static boolean timerStarted;
	private static double timeElapsed;
	
	/**
	 * Initialises the game onto the frame, calls the maze generator and
	 * generally runs the slide ninja slide game
	 * 
	 * @param frame
	 *            the frame on which the game will be displayed
	 * @param difficulty
	 *            the difficulty chosen for the game(for generation)
	 */
	public static void play (JFrame frame, SNSDifficulty difficulty) {
		initialiseFields();
		frame.getContentPane().removeAll();
		// ice colour is pale turquoise
		frame.getContentPane().setBackground(new Color(0, 206, 209));
		GamePanel gamePanel = new GamePanel();

		// generate map
		final SNSMap map = SNSMazeGenerator.generateMap(difficulty);
		// generate enemies
		final EnemyHorde horde = new EnemyHorde(
				SNSMazeGenerator.generateEnemies(difficulty, map));
		horde.cullTheWeak(map); // deletes enemies that are unfortunate enough
								// to spawn in a wall

		gamePanel.addObject(map);
		gamePanel.addObject(horde);
		gamePanel.setPreferredSize(new Dimension(900, 900));
		gamePanel.setOpaque(false);

		frame.add(gamePanel, BorderLayout.CENTER);
		ActionListener quit = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				quit();
			}
		};

		SNSGui gui = new SNSGui(quit);
		frame.add(gui, BorderLayout.EAST);

		frame.pack();
		frame.setResizable(false);

		final Player player1 = new Player(difficulty, true); // true, player has
		final Player player2 = new Player(difficulty, true); // black border
		player2.setColour(Color.RED);
		player1.setColour(Color.ORANGE);
		player2.setY(player2.getY() + player1.getHeight());
		player1.setY(player1.getY() - player1.getHeight());
		final SNSMovement ninja1 = new SNSMovement(player1, map.getAllWalls(),
				map.getAllSafeTiles(), horde, player2);
		final SNSMovement ninja2 = new SNSMovement(player2, map.getAllWalls(),
				map.getAllSafeTiles(), horde, player1);

		Cheese cheese = new Cheese(difficulty);
		gamePanel.addObject(cheese);		
		gamePanel.addObject(player1);
		gamePanel.addObject(player2);
		
		final KeyListener readKey = new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (!timerStarted) {
					setTimer();
					timerStarted = true;
				}
				int key = e.getKeyCode();
				if (key == 'W') {
					ninja1.up(true);
				}
				if (key == 'A') {
					ninja1.left(true);
				}
				if (key == 'S') {
					ninja1.down(true);
				}
				if (key == 'D') {
					ninja1.right(true);
				}
				if (key == KeyEvent.VK_SPACE) {
					tryThrust(1, ninja1);
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				int key = e.getKeyCode();
				if (key == 'W') {
					ninja1.up(false);
				}
				if (key == 'A') {
					ninja1.left(false);
				}
				if (key == 'S') {
					ninja1.down(false);
				}
				if (key == 'D') {
					ninja1.right(false);
				}
			}

			@Override
			public void keyTyped(KeyEvent e) {
			}
		};
		final KeyListener readKey2 = new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (!timerStarted) {
					setTimer();
					timerStarted = true;
				}
				int key = e.getKeyCode();
				if (key == KeyEvent.VK_UP || key == 'I') {
					ninja2.up(true);
				}
				if (key == KeyEvent.VK_LEFT || key == 'J') {
					ninja2.left(true);
				}
				if (key == KeyEvent.VK_DOWN || key == 'K') {
					ninja2.down(true);
				}
				if (key == KeyEvent.VK_RIGHT || key == 'L') {
					ninja2.right(true);
				}
				if (key == KeyEvent.VK_ENTER) {
					tryThrust(2, ninja2);
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				int key = e.getKeyCode();
				if (key == KeyEvent.VK_UP || key == 'I') {
					ninja2.up(false);
				}
				if (key == KeyEvent.VK_LEFT || key == 'J') {
					ninja2.left(false);
				}
				if (key == KeyEvent.VK_DOWN || key == 'K') {
					ninja2.down(false);
				}
				if (key == KeyEvent.VK_RIGHT || key == 'L') {
					ninja2.right(false);
				}
			}

			@Override
			public void keyTyped(KeyEvent e) {
			}
		};
		frame.addKeyListener(readKey);
		frame.addKeyListener(readKey2);
		frame.requestFocus();

		boolean won = false;

		System.out.println("Player 1: WASD to move, SPACE to thrust");
		System.out.println("Player 2: IJKL or arrow keys, ENTER to thrust");

		while (won == false && playing) {
			if (checkloss(ninja1, ninja2)) {
				SNSDefeat.show(frame, ninja1.getDeathCount(),
						ninja2.getDeathCount(), timeElapsed);
				break;
			}

			gui.update(timeElapsed, cooldownLength - cooldownElapsed[0] + 1,
					cooldownLength - cooldownElapsed[1] + 1,
					ninja1.getDeathCount(), ninja2.getDeathCount());

			ninja1.move();
			ninja2.move();
			horde.move();
			frame.repaint();
			try {
				Thread.sleep(20); // 50fps
			} catch (InterruptedException e1) {
			}
			if (player1.hitTest(cheese) || player2.hitTest(cheese)) {
				won = true;
				System.out.println("Treasure obtained!");
			}
		}

		stopTimer();
		if (won == true) {
			// wait for 0.2 seconds before going to victory screen
			long endTime = System.currentTimeMillis();
			while (System.currentTimeMillis() - endTime < 200) {
				ninja2.move();
				ninja1.move();
				horde.move();

				frame.repaint();
				try {
					Thread.sleep(20); // 50fps
				} catch (InterruptedException e1) {
				}
			}
			SNSVictory.show(frame, ninja1.getDeathCount(),
					ninja2.getDeathCount(), timeElapsed);
		}
	}

	/**
	 * Checks if both players are dead to determine if the game is lost
	 * 
	 * @param p1
	 *            the first player to be checked
	 * @param p2
	 *            the second player to be checked
	 * @return true if both players are dead, false if otherwise
	 */
	private static boolean checkloss(SNSMovement p1, SNSMovement p2) {
		if (!p1.isPlayerAlive() && !p2.isPlayerAlive()) {
			return true;
		}
		return false;
	}

	/*
	 * sets the timer to start counting every 0.1s
	 */
	private static void setTimer() {
		gameTimer = new Timer();
		TimerTask gameTimerTick = new TimerTask() {
			@Override
			public void run() {
				timeElapsed += 0.1;
			}
		};
		gameTimer.schedule(gameTimerTick, 0, 100);
	}

	/*
	 * Returns how much time has passed since the timer was started
	 */
	protected static double getGameTimeElapsed() {
		return timeElapsed;
	}

	/*
	 * initialise fields
	 */
	private static void initialiseFields() {
		playing = true;
		timerStarted = false;
		timeElapsed = 0;
		cooldownLength = 10;
		cooldownElapsed = new int[2];
		java.util.Arrays.fill(cooldownElapsed, 0);
		ready = new boolean[2];
		java.util.Arrays.fill(ready, true);
	}

	/*
	 * If thrust is not on cooldown, specified player thrusts and a cooldown is
	 * started. Otherwise, does nothing.
	 */
	protected static void tryThrust(int playerNumber, final SNSMovement ninja) {
		final int playerIndex = playerNumber - 1; // for purposes of using an
													// array
		// if thrust ready
		if (ready[playerIndex] && ninja.isPlayerAlive()) {
			// set ready to false until cooldown is over
			ready[playerIndex] = false;
			// turn on thrust
			System.out.println("Player" + playerNumber + " thrust on");
			ninja.thrust(true);

			// schedule to turn off thrust in one second
			final Timer timer = new Timer();
			TimerTask thrustOff = new TimerTask() {
				@Override
				public void run() {
					ninja.thrust(false);
					timer.cancel();
				}
			};
			timer.schedule(thrustOff, 1000); // 1000 ms

			// set up cooldown
			cooldownElapsed[playerIndex] = 0;
			final Timer cooldownTimer = new Timer();
			// every tick, cooldown elapsed increases until cooldown elapsed
			// has reached cooldown length
			TimerTask cooldownTick = new TimerTask() {
				@Override
				public void run() {
					if (cooldownElapsed[playerIndex] == cooldownLength) {
						ready[playerIndex] = true; // set ready to true
						cooldownElapsed[playerIndex]++;
						cooldownTimer.cancel();
					} else {
						cooldownElapsed[playerIndex]++;
					}
				}
			};
			cooldownTimer.schedule(cooldownTick, 0, 1000); // ticks every 1000ms

		} else {
			if (ninja.isPlayerAlive()) {
				// "PlayerX's thrust is on cooldown, 10 seconds remaining."
				System.out.println("Player" + playerNumber
						+ "'s thrust is on cooldown, "
						+ (cooldownLength - cooldownElapsed[playerIndex] + 1)
						+ " seconds remaining");
			} else {
				System.out.println("Cannot use thrust while dead!");
			}
		}

	}

	/**
	 * Sets the field playing to false, which causes the main method of this
	 * function stop the playing loop and return.
	 */
	protected static void quit() {
		// lazy fix for cooldown timer not cancelling upon quit
		cooldownElapsed[0] = cooldownLength;
		cooldownElapsed[1] = cooldownLength;
		playing = false;
	}

	private static void stopTimer() {
		if (timerStarted) {
			gameTimer.cancel();
			gameTimer.purge();
		}
	}
}
