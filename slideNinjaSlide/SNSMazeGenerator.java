package slideNinjaSlide;
import java.util.ArrayList;
import java.util.Random;

import enemyAI.DrunkCircularPatrolAI;
import enemyAI.DiagonalPatrolAI;
import enemyAI.EnemyAI;
import enemyAI.HorizontalPatrolAI;
import enemyAI.VerticalPatrolAI;
import enums.Direction;
import enums.SNSDifficulty;
import gameObjects.Enemy;
import gameObjects.SNSMap;
import gameObjects.SafeTile;
import gameObjects.Tile;

/**This is the class in charge of the generation of the maps for the slide ninja slide game mode
 * @author Jeffrey
 *
 */
public class SNSMazeGenerator {
	/**generates and returns the map for the slide ninja slide game
	 * @param difficulty the difficulty chosen for the game
	 * @return map
	 */
	public static SNSMap generateMap(SNSDifficulty difficulty) {
		SNSMap map = new SNSMap(generateBorders(difficulty),
				generateWalls(difficulty), generateRandomBlocks(difficulty),
				generateSafeTiles(difficulty));
		return map;
	}

	/*
	 * We are trying to make a spiral maze, like this. Start at top left, try to
	 * get to the centre.
	 * 
	 * 		* * * * * * * * *
	 * 		*____________   * 
	 * 		*            |  * 
	 * 		*   ______   |  * 
	 *		*   |    |   |  * 
	 * 		*   |    |   |  *
	 * 		*   |________|  * 
	 * 		*               * 
	 *		* * * * * * * * *
	 */

	private static ArrayList<Tile> generateWalls(SNSDifficulty difficulty) {
		// make a 2D array representing all the tiles in the map
		// starting from (1, 1), move right until we hit a border or a tile
		// that we have already walled off.
		// then turn 90 degrees clockwise, and move down until we hit a border
		// or a tile that we have already walled off.
		// continue that process, and end up with a spiral.

		int numRows = difficulty.getNumRows();
		int numCols = difficulty.getNumCols();
		int tileWidth = difficulty.getTileWidth();
		int wallThickness = difficulty.getWallThickness();

		// false if we need to wall off this tile, true otherwise
		// i.e., true if it's a border tile or we have already walled it off
		boolean[][] checked = new boolean[numRows][numCols];

		int i, j;
		for (i = 0; i < numRows; i++) {
			for (j = 0; j < numCols; j++) {
				checked[i][j] = false;
			}
		}

		ArrayList<Tile> wallTiles = new ArrayList<Tile>();

		int row = 0;
		int col = 0; // start at 1, 1
		int numTilesLeft = numRows * numCols;

		Direction direction = Direction.EAST;
		while (numTilesLeft > 0) {

			checked[row][col] = true; // mark tile visited

			if (nextTileChecked(row, col, direction, checked)) {
				// turn clockwise
				direction = direction.clockwise();
				// add this tiny square to make the walls seamless
				if (direction == Direction.WEST) {
					wallTiles.add(createWall(row, col, tileWidth, wallThickness, wallThickness,
							Direction.NORTH));
				}
			} else {
				wallTiles.add(createWall(row, col, tileWidth, tileWidth, wallThickness,
						direction.clockwise())); // make wall
			}

			// go forward
			// too much effort to wrap this in a method because java passes
			// by value
			if (direction == Direction.NORTH)
				row--;
			if (direction == Direction.SOUTH)
				row++;
			if (direction == Direction.EAST)
				col++;
			if (direction == Direction.WEST)
				col--;

			numTilesLeft--;
		}
		return wallTiles;
	}

	private static ArrayList<Tile> generateBorders(SNSDifficulty difficulty) {
		int numRows = difficulty.getNumRows();
		int numCols = difficulty.getNumCols();
		int tileWidth = difficulty.getTileWidth();
		int wallThickness = difficulty.getWallThickness();

		ArrayList<Tile> borders = new ArrayList<Tile>();
		// north
		borders.add(new Tile(0, 0, numCols*tileWidth, wallThickness));

		// south
		borders.add(new Tile(0, numRows*tileWidth - wallThickness, 
				numCols * tileWidth, wallThickness));

		// west
		borders.add(new Tile(0, 0, wallThickness, numRows*tileWidth));

		// east
		borders.add(new Tile(numCols*tileWidth, 0, wallThickness, 
				numRows*tileWidth));
		return borders;
	}
	
	private static ArrayList<SafeTile> generateSafeTiles(
			SNSDifficulty difficulty) {
		ArrayList<SafeTile> safeTiles = new ArrayList<SafeTile>();
		int i, j;
		int numRows = difficulty.getNumRows();
		int numCols = difficulty.getNumCols();
		int tileWidth = difficulty.getTileWidth();
		int wallThickness = difficulty.getWallThickness();
		for (i = 0; i < numRows; i++) {
			for (j = 0; j < numCols; j++) {
				if (coordinateIsSafe(i, j, difficulty)) {
					safeTiles.add(new SafeTile(j * tileWidth + wallThickness, 
							i * tileWidth + wallThickness,
							tileWidth - wallThickness, 
							tileWidth - wallThickness));
				}
			}
		}
		return safeTiles;
	}

	/*
	 * Returns whether this tile should be a safe tile or not
	 */
	private static boolean coordinateIsSafe(int i, int j,
			SNSDifficulty difficulty) {
		int numRows = difficulty.getNumRows();
		if (i == numRows/2 + 1) { // too close to the end for a safe tile
			return false; 
		}
		if (i == 0 && j == 0) { // starting tile
			return true;
		}
		if (i + j == numRows - 1) { // NorthEast / SouthWest diagonal
			return true;
		}
		if (i == j + 1 && i < numRows/2) { // NorthWest diagonal
			return true;
		}
		if (i == j && i > numRows/2) { // SouthEast diagonal
			return true;
		}
		return false;
	}

	private static ArrayList<Tile> generateRandomBlocks(
			SNSDifficulty difficulty) {
		ArrayList<Tile> randomBlocks = new ArrayList<Tile>();
		int i, j;
		int numRows = difficulty.getNumRows();
		int numCols = difficulty.getNumCols();
		int tileWidth = difficulty.getTileWidth();
		int wallThickness = difficulty.getWallThickness();
		int maxBlocks = numRows*numCols/2; // don't want too many random blocks
		int numBlocks = 0;
		int xInTile = 0;
		int yInTile = 0;
		for (i = 0; i < numRows; i++) {
			for (j = 0; j < numCols; j++) {
				// only want random blocks on unsafe tiles
				if (!coordinateIsSafe(i, j, difficulty)) {
					
					// 33% chance of generating a random block
					if (roll1D(3) == 1 && numBlocks < maxBlocks) {
						xInTile = (int) (Math.random()*(tileWidth - 4*wallThickness)) + 2*wallThickness;
						// take away 4*wallThickness so we don't have the block
						// at the edges of the tile
						yInTile = (int) (Math.random()*(tileWidth - 4*wallThickness)) + 2*wallThickness;
						int xMultiplier = 1;
						int yMultiplier = 1;
						// 50% to have long block, 50% to have tall block
						if (roll1D(2) == 1) {
							xMultiplier = 2;
						} else {
							yMultiplier = 2;
						}
						// x value: 
						// - j*tileWidth gives the correct tile column
						//   or border tiles
						// - xInTile is a randomises the x position in the tile
						randomBlocks.add(new Tile(j*tileWidth + xInTile, 
								i*tileWidth + yInTile, 
								wallThickness/2*xMultiplier, 
								wallThickness/2*yMultiplier));
						numBlocks++;
					}
				}
			}
		}
		return randomBlocks;
	}

	private static boolean nextTileChecked(int row, int col,
			Direction direction, boolean[][] checked) {
		// go forward
		if (direction == Direction.NORTH)
			row--;
		if (direction == Direction.SOUTH)
			row++;
		if (direction == Direction.EAST)
			col++;
		if (direction == Direction.WEST)
			col--;

		if (!inBounds(row, col, checked)) {
			return true;
		}
		// return whether this tile is checked
		return checked[row][col];
	}

	private static boolean inBounds(int row, int col, boolean[][] checked) {
		if (row < 0 || row >= checked.length || col < 0
				|| col >= checked.length) {
			return false;
		}
		return true;
	}

	private static Tile createWall(int row, int col, int mapTileWidth,
			int thisTileWidth, int wallThickness, Direction direction) {
		int width;
		int height;
		int x = 0;
		int y = 0;
		if (direction == Direction.NORTH || direction == Direction.SOUTH) {
			width = thisTileWidth;
			height = wallThickness; // same thickness as player to prevent
									// movement through
			// walls. This is hardcoded atm.
		} else {
			width = wallThickness;
			height = thisTileWidth;
		}

		if (direction == Direction.NORTH) {
			x = col * mapTileWidth;
			y = row * mapTileWidth;// - height/2; // aligns the centre of this
									// wall with
									// the border line between tiles
		}
		if (direction == Direction.SOUTH) {
			x = col * mapTileWidth;
			y = (row + 1) * mapTileWidth;// + height/2;
		}
		if (direction == Direction.WEST) {
			x = col * mapTileWidth;// - width/2;
			y = row * mapTileWidth;
		}
		if (direction == Direction.EAST) {
			x = (col + 1) * mapTileWidth;// + width/2;
			y = row * mapTileWidth;
		}

		return new Tile(row, col, x, y, width, height);
	}

	/**Generates a set of enemies to be placed on the map in order defeat a player. Amount of enemies depends on difficulty
	 * @param difficulty to determine amount of enemies
	 * @param map the map to place enemies on
	 * @return an ArrayList of enemies
	 */
	public static ArrayList<Enemy> generateEnemies(SNSDifficulty difficulty, SNSMap map) {
		ArrayList<Enemy> enemies = new ArrayList<Enemy>();
		int i, j;
		int numRows = difficulty.getNumRows();
		int numCols = difficulty.getNumCols();
		int tileWidth = difficulty.getTileWidth();
		int wallThickness = difficulty.getWallThickness();
		int enemySize = difficulty.getPlayerSize()*3/2;
		int maxEnemies = numRows*numCols; // no limit atm
		int numEnemies = 0;
		int enemySpeed = difficulty.getEnemySpeed();
		int tilesSinceEnemy = 0;
		
		for (i = 0; i < numRows; i++) {
			for (j = 0; j < numCols; j++) {
				// only want random blocks on unsafe tiles
				if (!coordinateIsSafe(i, j, difficulty)) {
					// pseudo random
					// starts off at 25% chance
					// if an enemy isn't generated, chance increases
					// can't have 4 empty tiles in a row
					int diceRoll = roll1D(4);
					if ((tilesSinceEnemy+1 >= diceRoll) 
							&& numEnemies < maxEnemies) {
						// take away 2*wall thickness + enemySize so we don't
						// spawn in a wall
						enemies.add(generateEnemy(i, j, enemySize, tileWidth,
								wallThickness, map, enemySpeed, difficulty));
						numEnemies++;
						tilesSinceEnemy = 0;
					} else {
						tilesSinceEnemy++;
					}
				}
			}
		}
		
		// guardians of the tiles just before the end
		i = numRows/2;
		j = numCols/2-1;
		enemies.add(generateEnemy(i, j, enemySize, tileWidth, wallThickness,
				map, enemySpeed, difficulty));
		enemies.add(generateEnemy(i+1, j, enemySize, tileWidth, wallThickness,
				map, enemySpeed, difficulty));
		enemies.add(generateEnemy(i+1, j+1, enemySize, tileWidth, wallThickness,
				map, enemySpeed, difficulty));
		
		return enemies;
	}
	
	private static Enemy generateEnemy(int row, int col, int enemySize,
			int tileWidth, int wallThickness, SNSMap map, int enemySpeed,
			SNSDifficulty difficulty) {
		int xInTile = (int) (Math.random()*(tileWidth 
				- (2*wallThickness + enemySize))) + wallThickness;
		int yInTile = (int) (Math.random()*(tileWidth 
				- (2*wallThickness + enemySize))) + wallThickness;

		EnemyAI chosenAI = null;
		

		// 35% vertical 35% horizontal 20% diagonal 10% spastic
		int diceRoll = roll1D(100);
		if (diceRoll <= 35) {
			chosenAI = new VerticalPatrolAI(map, difficulty);
		} else if (diceRoll <= 70) {
			chosenAI = new HorizontalPatrolAI(map, difficulty);
		} else if (diceRoll <= 90) {
			chosenAI = new DiagonalPatrolAI(map, difficulty);
		} else {
			chosenAI = new DrunkCircularPatrolAI(map, difficulty);
		}
		
		// x value: 
		// - j*tileWidth gives the correct tile column
		// - xInTile is a randomises the x position in the tile
		return new Enemy(col * tileWidth + xInTile, 
				row * tileWidth + yInTile, enemySize, chosenAI);
	}

	/*
	 * Simulates the rolling of an X sided die.
	 * (Returns a number between 1-X inclusive)
	 */
	private static int roll1D(int X) {
		long seed = System.nanoTime();
		Random rand = new Random(seed);
		return rand.nextInt(X) + 1;
	}
}