package enemyAI;

import java.awt.Graphics2D;
import java.util.ArrayList;

import enums.SNSDifficulty;
import gameObjects.Enemy;
import gameObjects.MazeObject;
import gameObjects.SNSMap;

/**
 * Patrols between bordering walls and safetiles in a diagonal fashion
 * 
 * @author Jeffrey
 * 
 */
public class DiagonalPatrolAI implements EnemyAI {

	private ArrayList<MazeObject> wallObjects;
	private SNSDifficulty difficulty;
	private int lastDirectionChange;

	public DiagonalPatrolAI(SNSMap map, SNSDifficulty difficulty) {
		wallObjects = new ArrayList<MazeObject>();
		wallObjects.addAll(map.getAllSafeTiles());
		wallObjects.addAll(map.getAllWalls());
		this.difficulty = difficulty;
		lastDirectionChange = 0;
	}

	@Override
	public double calculateDx(Enemy enemy) {
		double dx = enemy.getDx();
		if (lastDirectionChange == 5) {
			dx *= -1;
		} else {
			for (MazeObject tile : wallObjects) {
				if (enemy.hitTest(tile)) {
					dx *= -1;
					break;
				}
			}
		}
		lastDirectionChange++;
		return dx;
	}

	@Override
	public double calculateDy(Enemy enemy) {
		double dy = enemy.getDy();
		if (lastDirectionChange == 10) {
			dy *= -1;
			lastDirectionChange = 0;
		} else {
			for (MazeObject tile : wallObjects) {
				if (enemy.hitTest(tile)) {
					dy *= -1;
					break;
				}
			}
		}
		lastDirectionChange++;

		return dy;
	}

	@Override
	public void initialiseEnemy(Enemy enemy) {
		int startingType = (int) (Math.random() * 2) + 1; // 1 - 2
		int speed = 1;
		if (difficulty == SNSDifficulty.HARD) {
			speed = 2;
		}
		if (startingType == 1) {
			enemy.setDx(speed);
			enemy.setDy(speed);
		} else if (startingType == 2) {
			enemy.setDx(-speed);
			enemy.setDy(speed);
		} else if (startingType == 3) {
			enemy.setDx(speed);
			enemy.setDy(-speed);
		} else {
			enemy.setDx(-speed);
			enemy.setDy(-speed);
		}

		// diagonal patrol will only be hitting 2 objects.
		// so find them, and remove all unnecessary objects from wallObjects
		class Shadow extends MazeObject {
			private static final long serialVersionUID = 1L;
			boolean botRight;

			public Shadow(int x, int y, int width, int height, boolean botRight) {
				this.x = x;
				this.y = y;
				this.width = width;
				this.height = height;
				this.botRight = botRight;
			}

			@Override
			public void draw(Graphics2D g2) {
			}

			public void addXY(int n) {
				y = y + n;
				if (botRight) {
					x = x + n;
				} else {
					x = x - n;
				}
			}
		}
		// is our bot travelling in the TopLeft-BotRight diagonal, or the
		// TopRight-BotLeft diagonal?
		boolean botRight = true;
		if (startingType == 2 || startingType == 3) {
			botRight = false;
		}
		Shadow shadow = new Shadow(enemy.getX(), enemy.getY(),
				enemy.getWidth(), enemy.getHeight(), botRight);
		MazeObject bound1 = null;
		MazeObject bound2 = null;
		int dMoved = 0;

		// move forward until we hit an object, our first bound
		while (bound1 == null) {
			for (MazeObject obj : wallObjects) {
				if (shadow.hitTest(obj)) {
					bound1 = obj;
				}
			}
			shadow.addXY(1);
			dMoved++;
		}

		shadow.addXY(-dMoved); // reset x, y to start

		// move back until we hit an object, our second bound
		while (bound2 == null) {
			for (MazeObject obj : wallObjects) {
				if (shadow.hitTest(obj)) {
					bound2 = obj;
				}
			}
			shadow.addXY(-1);
		}

		wallObjects.clear();
		wallObjects.add(bound1);
		wallObjects.add(bound2);
	}
}
