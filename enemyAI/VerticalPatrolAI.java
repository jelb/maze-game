package enemyAI;

import java.awt.Graphics2D;
import java.util.ArrayList;

import enums.SNSDifficulty;
import gameObjects.Enemy;
import gameObjects.MazeObject;
import gameObjects.SNSMap;

/**
 * Patrols between bordering walls and safetiles in a Vertical only type patrol
 * 
 * @author Jeffrey
 * 
 */
public class VerticalPatrolAI implements EnemyAI {
	private ArrayList<MazeObject> wallObjects;
	private SNSDifficulty difficulty;

	public VerticalPatrolAI(SNSMap map, SNSDifficulty difficulty) {
		wallObjects = new ArrayList<MazeObject>();
		wallObjects.addAll(map.getAllSafeTiles());
		wallObjects.addAll(map.getAllWalls());
		this.difficulty = difficulty;
	}

	@Override
	public double calculateDx(Enemy enemy) {
		return enemy.getDx();
	}

	@Override
	public double calculateDy(Enemy enemy) {
		double dy = enemy.getDy();
		for (MazeObject tile : wallObjects) {
			if (enemy.hitTest(tile)) {
				dy *= -1;
				break;
			}
		}
		enemy.setY(enemy.getY() + (int) dy);
		return dy;
	}

	@Override
	public void initialiseEnemy(Enemy enemy) {
		if ((int) (Math.random() * 2) == 1) {
			enemy.setDy(difficulty.getEnemySpeed());
		} else {
			enemy.setDy(-difficulty.getEnemySpeed());
		}

		// vertical patrol will only be hitting 2 objects.
		// so find them, and remove all unnecessary objects from wallObjects
		class Shadow extends MazeObject {
			private static final long serialVersionUID = 1L;

			public Shadow(int x, int y, int width, int height) {
				this.x = x;
				this.y = y;
				this.width = width;
				this.height = height;
			}

			@Override
			public void draw(Graphics2D g2) {
			}

			public void addY(int n) {
				y = y + n;
			}
		}
		Shadow shadow = new Shadow(enemy.getX(), enemy.getY(),
				enemy.getWidth(), enemy.getHeight());
		MazeObject bound1 = null;
		MazeObject bound2 = null;
		int yMoved = 0;

		// move right until we hit an object, our first bound
		while (bound1 == null) {
			for (MazeObject obj : wallObjects) {
				if (shadow.hitTest(obj)) {
					bound1 = obj;
				}
			}
			shadow.addY(1);
			yMoved++;
		}

		shadow.addY(-yMoved); // reset y to start

		// move left until we hit an object, our second bound
		while (bound2 == null) {
			for (MazeObject obj : wallObjects) {
				if (shadow.hitTest(obj)) {
					bound2 = obj;
				}
			}
			shadow.addY(-1);
		}

		wallObjects.clear();
		wallObjects.add(bound1);
		wallObjects.add(bound2);
	}
}
