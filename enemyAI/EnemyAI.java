package enemyAI;

import gameObjects.Enemy;

/**
 * The interface for the different type of enemies
 * 
 * @author Jeffrey
 * 
 */
public interface EnemyAI {
	/**
	 * Calculates their next change in x-coordinate in order to update their
	 * position
	 * 
	 * @param enemy
	 *            the enemy whose x-coordinate is to be updated
	 * @return the change in x-position
	 */
	public double calculateDx(Enemy enemy);

	/**
	 * Calculates their next change in y-coordinate in order to update their
	 * position
	 * 
	 * @param enemy
	 *            the enemy whose y-coordinate is to be updated
	 * @return the change in y-position
	 */
	public double calculateDy(Enemy enemy);

	/**
	 * Initialises the enemies starting positions, speed etc
	 * 
	 * @param enemy
	 *            the enemy to initialise
	 */
	public void initialiseEnemy(Enemy enemy);
}
