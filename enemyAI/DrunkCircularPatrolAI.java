package enemyAI;

import java.util.ArrayList;
import java.util.Random;

import enums.SNSDifficulty;
import gameObjects.Enemy;
import gameObjects.MazeObject;
import gameObjects.SNSMap;

public class DrunkCircularPatrolAI implements EnemyAI {
	private ArrayList<MazeObject> wallObjects;
	private SNSDifficulty difficulty;
	private int xMovedCount;
	private int yMovedCount;

	public DrunkCircularPatrolAI(SNSMap map, SNSDifficulty difficulty) {
		wallObjects = new ArrayList<MazeObject>();
		wallObjects.addAll(map.getAllSafeTiles());
		wallObjects.addAll(map.getAllWalls());
		this.difficulty = difficulty;
		xMovedCount = 0;
		yMovedCount = difficulty.getTileWidth()/(2);
	}
	
	@Override
	public double calculateDx(Enemy enemy) {
		double dx = enemy.getDx();
		xMovedCount++;
		if (xMovedCount == difficulty.getTileWidth()) {
			xMovedCount = 0;
			dx = -dx;
			return dx;
		}
		for (MazeObject obj : wallObjects) {
			if (enemy.hitTest(obj)) {
				dx = -dx;
				break;
			}
		}
		return dx;
	}

	@Override
	public double calculateDy(Enemy enemy) {
		double dy = enemy.getDy();
		yMovedCount++;
		if (yMovedCount == difficulty.getTileWidth()) {
			yMovedCount = 0;
			dy = -dy;
			return dy;
		}
		for (MazeObject obj : wallObjects) {
			if (enemy.hitTest(obj)) {
				dy = -dy;
				break;
			}
		}
		return dy;
	}

	@Override
	public void initialiseEnemy(Enemy enemy) {
		long seed = System.nanoTime();
		Random rand = new Random(seed);
		int dice = rand.nextInt(4);
		if (dice == 1) {
			enemy.setDx(1);
			enemy.setDy(1);
		} else if (dice == 2) {
			enemy.setDx(-1);
			enemy.setDy(1);
		} else if (dice == 3) {
			enemy.setDx(1);
			enemy.setDy(-1);
		} else {
			enemy.setDx(-1);
			enemy.setDy(-1);
		}
	}
}
