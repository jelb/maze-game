package enemyAI;

import java.awt.Graphics2D;
import java.util.ArrayList;

import enums.SNSDifficulty;
import gameObjects.Enemy;
import gameObjects.MazeObject;
import gameObjects.SNSMap;

/**
 * Patrols between bordering walls and safetiles in a Horizontal only type
 * patrol
 * 
 * @author Jeffrey
 * 
 */
public class HorizontalPatrolAI implements EnemyAI {
	private ArrayList<MazeObject> wallObjects;
	private SNSDifficulty difficulty;

	public HorizontalPatrolAI(SNSMap map, SNSDifficulty difficulty) {
		wallObjects = new ArrayList<MazeObject>();
		wallObjects.addAll(map.getAllSafeTiles());
		wallObjects.addAll(map.getAllWalls());
		this.difficulty = difficulty;
	}

	@Override
	public double calculateDx(Enemy enemy) {
		double dx = enemy.getDx();
		for (MazeObject tile : wallObjects) {
			if (enemy.hitTest(tile)) {
				dx *= -1;
				break;
			}
		}
		enemy.setX(enemy.getX() + (int) dx);
		return dx;
	}

	@Override
	public double calculateDy(Enemy enemy) {
		return enemy.getDy();
	}

	@Override
	public void initialiseEnemy(Enemy enemy) {
		if ((int) (Math.random() * 2) == 1) {
			enemy.setDx(difficulty.getEnemySpeed());
		} else {
			enemy.setDx(-difficulty.getEnemySpeed());
		}

		// horizontal patrol will only be hitting 2 objects.
		// so find them, and remove all unnecessary objects from wallObjects
		class Shadow extends MazeObject {
			private static final long serialVersionUID = 1L;

			public Shadow(int x, int y, int width, int height) {
				this.x = x;
				this.y = y;
				this.width = width;
				this.height = height;
			}

			@Override
			public void draw(Graphics2D g2) {
			}

			public void addX(int n) {
				x = x + n;
			}
		}
		Shadow shadow = new Shadow(enemy.getX(), enemy.getY(),
				enemy.getWidth(), enemy.getHeight());
		MazeObject bound1 = null;
		MazeObject bound2 = null;
		int xMoved = 0;

		// move right until we hit an object, our first bound
		while (bound1 == null) {
			for (MazeObject obj : wallObjects) {
				if (shadow.hitTest(obj)) {
					bound1 = obj;
				}
			}
			shadow.addX(1);
			xMoved++;
		}

		shadow.addX(-xMoved); // reset x to start

		// move left until we hit an object, our second bound
		while (bound2 == null) {
			for (MazeObject obj : wallObjects) {
				if (shadow.hitTest(obj)) {
					bound2 = obj;
				}
			}
			shadow.addX(-1);
		}

		wallObjects.clear();
		wallObjects.add(bound1);
		wallObjects.add(bound2);
	}
}
