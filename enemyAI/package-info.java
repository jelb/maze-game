/** This package contains all the classes which determine the patrol type of 
 * each enemy in the Slide Ninja Slide game mode
 * 
 * @author Jeffrey
 *
 */
package enemyAI;