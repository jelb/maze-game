package pages;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.text.DecimalFormat;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import regularMaze.HighScoreManager;

/**
 * This is the game over page of the normal game if the player has won. Displays
 * a the time elapsed, player score, and if the game is valid or not.
 * 
 * @author Jeffrey
 * 
 */
public class Victory {
	private static boolean finish;

	public static void show(JFrame frame, boolean gameValid,
			double timeElapsed, int score) {
		initialisePage(frame, gameValid, timeElapsed, score);
		while (finish == false) {
			// wait for other listeners to change the choice field
			Thread.yield();
		}
	}
	
	private static String highScore(boolean gameValid, int score) {		
		
		HighScoreManager highscore = new HighScoreManager();
		if(gameValid) {
			highscore.addScore(score);
		}
		String topScore = highscore.displayTopScores();
		//Debugging console
		for(int x = 0; x < 2; x++) {
			System.out.println(topScore);
		}
		
		return topScore;
	}

	private static void initialisePage(JFrame frame, boolean gameValid,
			double timeElapsed, int score) {
		for (KeyListener k : frame.getKeyListeners()) {
			frame.removeKeyListener(k);
		}
		;
		frame.getContentPane().removeAll();
		frame.getContentPane().setBackground(new Color(20, 50, 100));

		String time = new DecimalFormat("#.##").format(timeElapsed);
		finish = false;
		String valid;
		String validColour;
		if (gameValid) {
			valid = "VALID";
			validColour = "green";
		} else {
			valid = "INVALID";
			validColour = "red";
		}
		
		//GETTING TOP SCORES
		String scores[] = highScore(gameValid,score).split(" ");
		int score1 = Integer.parseInt(scores[0]);
		int score2 = Integer.parseInt(scores[1]);
		int score3 = Integer.parseInt(scores[2]);
		int score4 = Integer.parseInt(scores[3]);
		int score5 = Integer.parseInt(scores[4]);

		// TITLE
		JPanel titlePanel = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		JLabel title = new JLabel("Congrats! You win!");
		title.setFont(new Font("Comic Sans MS", Font.BOLD, 60));
		title.setForeground(Color.ORANGE);
		titlePanel.add(title, c);
		titlePanel.setBorder(new EmptyBorder(20, 0, 0, 0));
		titlePanel.setBackground(null);

		// CONTENT
		JPanel contentPanel = new JPanel(new GridBagLayout());
		JLabel analysis = new JLabel(
				"<html><div style=\"text-align: center;\">" + "You took <b>"
						+ time + "</b> seconds!<br>" + "Your score is <b>"
						+ score + "</b>!<br>"
						+ "Your time and score are <b><font color='"
						+ validColour + "'>" + valid + "</font>!</b>"
						+ "<br><br><b>Top Scores</b><br>"  
						+ "<br><b>" + score1 + "</b><br>" 
						+ "<br><b>" + score2 + "</b><br>" 
						+ "<br><b>" + score3 + "</b><br>" 
						+ "<br><b>" + score4 + "</b><br>" 
						+ "<br><b>" + score5 + "</b><br>" 
						+ "</div>");
		if (gameValid) {
			analysis.setText(analysis.getText() + "</html>");
		} else {
			analysis.setText(analysis.getText()
					+ "<br><br><br>"
					+ "Note: Your score is invalid because you increased your <br>"
					+ "speed to 3, used at least one hint, or loaded the game."
					+ "</html>");
		}
		analysis.setFont(new Font("Arial", Font.PLAIN, 20));
		analysis.setForeground(Color.ORANGE);
		c = new GridBagConstraints();
		c.insets = new Insets(0, 0, 0, 0);
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.fill = GridBagConstraints.BOTH;

		contentPanel.add(analysis, c);
		contentPanel.setBackground(null);
		contentPanel.setPreferredSize(new Dimension(1000, 500));

		// MAIN MENU BUTTON
		JPanel buttonPanel = new JPanel(new GridBagLayout());
		JButton menuButton = new JButton("Main Menu");
		menuButton.setFont(new Font("Arial", Font.PLAIN, 40));
		menuButton.setPreferredSize(new Dimension(400, 100));
		menuButton.setFocusPainted(false);
		c = new GridBagConstraints();

		ActionListener menuButtonClick = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				finish = true;
			}
		};
		menuButton.addActionListener(menuButtonClick);
		buttonPanel.add(menuButton, c);
		buttonPanel.setBorder(new EmptyBorder(0, 0, 60, 0));
		buttonPanel.setBackground(null);

		frame.add(titlePanel, BorderLayout.NORTH);
		frame.add(contentPanel, BorderLayout.CENTER);
		frame.add(buttonPanel, BorderLayout.SOUTH);
		frame.setPreferredSize(new Dimension(1200 + frame.getInsets().left
				+ frame.getInsets().right, 900 + frame.getInsets().bottom
				+ frame.getInsets().top));
		frame.pack();
	}
}
