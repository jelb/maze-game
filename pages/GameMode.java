package pages;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import enums.Choice;

/**
 * This is the page on which you choose what type of game you wish to play, or
 * seek help for
 * 
 * @author Jeffrey
 * 
 */
public class GameMode {
	private static Choice choice;
	private static boolean back;

	/**
	 * Displays the page and determines the actions when a button is pressed
	 * 
	 * @param frame
	 *            The frame on which the page will be displayed
	 */
	public static void show(JFrame frame) {
		initialisePage(frame);

		// wait until a button is clicked
		while (back == false) {
			if (choice == Choice.SNS_HELP) {
				SNSHelp.show(frame);
				choice = null;
				initialisePage(frame);
			}
			if (choice == Choice.REGULAR_MAZE_HELP) {
				RegularMazeHelp.show(frame);
				choice = null;
				initialisePage(frame);
			}
			if (choice == Choice.SNS) {
				if (SNSDifficultyPage.show(frame)) {
					return; // page returns whether game was played.
							// if true, we want to return to
							// the main menu
							// otherwise (back button was pressed), we stay
							// on this page
				}
				choice = null;
				initialisePage(frame);
			}
			if (choice == Choice.REGULAR_MAZE) {
				if (RegularMazeDifficultyPage.show(frame)) {
					return;
				}
				choice = null;
				initialisePage(frame);
			}
			// wait for other listeners to change the choice or back fields
			Thread.yield();
		}

	}

	/**
	 * Initialises the page to be displayed
	 * 
	 * @param frame
	 */
	private static void initialisePage(JFrame frame) {
		choice = null;
		back = false;
		frame.getContentPane().removeAll();

		GridBagConstraints c = null;
		// TITLE
		JPanel titlePanel = new JPanel(new GridBagLayout());
		c = new GridBagConstraints();
		JLabel title = new JLabel("Select Game Mode");
		title.setFont(new Font("Comic Sans MS", Font.BOLD, 60));
		title.setForeground(Color.ORANGE);
		titlePanel.add(title, c);
		titlePanel.setBorder(new EmptyBorder(20, 0, 0, 0));

		// BUTTONS
		JPanel buttonPanel = new JPanel(new GridBagLayout());

		// REGULAR MAZE
		JButton regularMazeButton = new JButton("Play Regular Maze");
		regularMazeButton.setFont(new Font("Arial", Font.PLAIN, 40));
		regularMazeButton.setPreferredSize(new Dimension(500, 100));
		regularMazeButton.setFocusPainted(false);
		c = new GridBagConstraints();
		c.gridy = 0;
		c.gridx = 0;

		ActionListener regularMazeButtonClick = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				choice = Choice.REGULAR_MAZE;
			}
		};
		regularMazeButton.addActionListener(regularMazeButtonClick);
		buttonPanel.add(regularMazeButton, c);

		// REGULAR MAZE HELP
		JButton regularMazeHelpButton = new JButton("?");
		regularMazeHelpButton.setFont(new Font("Arial", Font.PLAIN, 60));
		regularMazeHelpButton.setPreferredSize(new Dimension(100, 100));
		regularMazeHelpButton.setFocusPainted(false);
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.WEST;
		c.gridy = 0;
		c.gridx = 1;

		ActionListener regularMazeHelpButtonClick = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				choice = Choice.REGULAR_MAZE_HELP;
			}
		};
		regularMazeHelpButton.addActionListener(regularMazeHelpButtonClick);
		buttonPanel.add(regularMazeHelpButton, c);

		// SLIDE NINJA SLIDE
		JButton SNSButton = new JButton("Play Slide Ninja Slide");
		SNSButton.setFont(new Font("Arial", Font.PLAIN, 40));
		SNSButton.setPreferredSize(new Dimension(500, 100));
		SNSButton.setFocusPainted(false);
		c = new GridBagConstraints();
		c.insets = new Insets(200, 0, 0, 0);
		c.gridy = 1;

		ActionListener SNSButtonClick = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				choice = Choice.SNS;
			}
		};
		SNSButton.addActionListener(SNSButtonClick);
		buttonPanel.add(SNSButton, c);

		// SLIDE NINJA SLIDE HELP
		JButton SNSHelpButton = new JButton("?");
		SNSHelpButton.setFont(new Font("Arial", Font.PLAIN, 60));
		SNSHelpButton.setPreferredSize(new Dimension(100, 100));
		SNSHelpButton.setFocusPainted(false);
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.WEST;
		c.insets = new Insets(200, 0, 0, 0);
		c.gridy = 1;
		c.gridx = 1;

		ActionListener SNSHelpButtonClick = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				choice = Choice.SNS_HELP;
			}
		};
		SNSHelpButton.addActionListener(SNSHelpButtonClick);
		buttonPanel.add(SNSHelpButton, c);

		// BACK BUTTON PANEL
		JPanel backPanel = new JPanel(new GridBagLayout());
		JButton backButton = new JButton("Back");
		backButton.setFont(new Font("Arial", Font.PLAIN, 40));
		backButton.setPreferredSize(new Dimension(200, 100));
		backButton.setFocusPainted(false);
		c = new GridBagConstraints();

		ActionListener backButtonClick = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				back = true;
			}
		};
		backButton.addActionListener(backButtonClick);
		backPanel.add(backButton, c);
		backPanel.setBorder(new EmptyBorder(0, 0, 20, 0));
		backPanel.setBackground(null);

		titlePanel.setBackground(null);
		buttonPanel.setBackground(null);
		frame.add(titlePanel, BorderLayout.NORTH);
		frame.add(buttonPanel, BorderLayout.CENTER);
		frame.add(backPanel, BorderLayout.SOUTH);
		frame.setPreferredSize(new Dimension(1200 + frame.getInsets().left
				+ frame.getInsets().right, 900 + frame.getInsets().bottom
				+ frame.getInsets().top));
		frame.pack();
	}
}
