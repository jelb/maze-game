package pages;

import enums.RegularMazeDifficulty;
import gameObjects.Cheese;
import gameObjects.Maze;
import gameObjects.Player;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileReader;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import regularMaze.PlayRegularMaze;
import regularMaze.RegularMazeGameFields;

/**
 * This is the page where you can load previously saved games to continue
 * playing
 * 
 * @author Jeffrey
 * 
 */
public class LoadPage {
	private static int saveFileNumber;
	private static boolean back;

	/**
	 * Displays the page and determines which game to load when a button is
	 * pressed
	 * 
	 * @param frame
	 */
	public static void show(JFrame frame) {
		initialisePage(frame);

		while (back == false) {
			if (saveFileNumber != 0) { // only supports one save atm
				RegularMazeGameFields fields = loadGameFields();
				if(fields != null){
					PlayRegularMaze.play(frame, fields);
					back = true; //FIXED Load looping quit problem.
				}
			}
			// wait for other listeners to change the choice field
			Thread.yield();
		}
	}

	private static void initialisePage(JFrame frame) {
		frame.getContentPane().removeAll();

		GridBagConstraints c = null;
		saveFileNumber = 0;
		back = false;

		// TITLE
		JPanel titlePanel = new JPanel(new GridBagLayout());
		c = new GridBagConstraints();
		JLabel title = new JLabel("Load Your Save Game");
		title.setFont(new Font("Comic Sans MS", Font.BOLD, 60));
		title.setForeground(Color.YELLOW);
		titlePanel.add(title, c);
		titlePanel.setBorder(new EmptyBorder(20, 0, 0, 0));
		

		// LOAD OPTIONS
		JPanel loadPanel = new JPanel(new GridBagLayout());

		// SAVE 
		JButton save = new JButton("Load");
		save.setFont(new Font("Arial", Font.PLAIN, 40));
		save.setPreferredSize(new Dimension(200, 100));
		save.setFocusPainted(false);
		c.gridy = 1;
		c.weightx = 0.5;
		c.insets = new Insets(75, 350, 0, 350);
		
		ActionListener save2ButtonClick = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				saveFileNumber = 2;
				try {
					Scanner sc = new Scanner(new FileReader("save.sav"));
					sc.close();
				} catch(Exception e){
					saveFileNumber = 0;
				}
			}
		};
		save.addActionListener(save2ButtonClick);
		loadPanel.add(save, c);

		// BACK BUTTON PANEL
		JPanel backPanel = new JPanel(new GridBagLayout());
		JButton backButton = new JButton("Back");
		backButton.setFont(new Font("Arial", Font.PLAIN, 40));
		backButton.setPreferredSize(new Dimension(200, 100));
		backButton.setFocusPainted(false);
		c = new GridBagConstraints();

		ActionListener backButtonClick = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				back = true;
			}
		};
		backButton.addActionListener(backButtonClick);
		backPanel.add(backButton, c);
		backPanel.setBorder(new EmptyBorder(0, 0, 20, 0));
		backPanel.setBackground(null);

		titlePanel.setBackground(null);
		loadPanel.setBackground(null);
		frame.add(titlePanel, BorderLayout.NORTH);
		frame.add(loadPanel, BorderLayout.CENTER);
		frame.add(backPanel, BorderLayout.SOUTH);
		frame.setPreferredSize(new Dimension(1200 + frame.getInsets().left
				+ frame.getInsets().right, 900 + frame.getInsets().bottom
				+ frame.getInsets().top));
		frame.pack();
	}

	/**
	 * Retrieves a previously saved game from its save file and loads it to the
	 * current game.
	 * 
	 * @return an arrayList of the different objects needed to re-initialise the
	 *         game
	 */
	private static RegularMazeGameFields loadGameFields() {
		RegularMazeGameFields mazeGameFields = null;

		String s1;
		String[] s2;

		// The elements of difficulty
		int numRows = 0;
		int numCols = 0;
		int tileWidth = 0;
		String[] mazeString = null;

		// The elements of Player
		int playerX = 0;
		int playerY = 0;
		int playerWidth = 0;
		int playerHeight = 0;
		boolean playerBorder = false;

		// The elements of Cheese
		int cheeseX = 0;
		int cheeseY = 0;
		int cheeseWidth = 0;
		int cheeseHeight = 0;

		// The time elapsed and fastest times, as strings
		double timeElapsed = 0;
		double fastestTime = 0;

		// The maze difficulty
		int difficultyOrdinal = 0;
		
		boolean gameValid = false;

		// Wrap all in a try/catch block to trap I/O errors.
		try {
			
				Scanner sc = new Scanner(new FileReader("save.sav"));

			while (sc.hasNextLine()) {
				s1 = sc.nextLine();
				s2 = s1.split(" ");

				if (s2[0].equals("Maze")) {
					if (s2[1].equals("numRows:")) {
						numRows = Integer.parseInt(s2[2]);
					} else if (s2[1].equals("numCols:")) {
						numCols = Integer.parseInt(s2[2]);
					} else if (s2[1].equals("tileWidth:")) {
						tileWidth = Integer.parseInt(s2[2]);
					} else if (s2[1].equals("map:")) {
						mazeString = s2;
					}
				} else if (s2[0].equals("Player")) {
					if (s2[1].equals("x:")) {
						playerX = Integer.parseInt(s2[2]);
					} else if (s2[1].equals("y:")) {
						playerY = Integer.parseInt(s2[2]);
					} else if (s2[1].equals("width:")) {
						playerWidth = Integer.parseInt(s2[2]);
					} else if (s2[1].equals("height:")) {
						playerHeight = Integer.parseInt(s2[2]);
					} else if (s2[1].equals("border:")) {
						playerBorder = Boolean.parseBoolean(s2[2]);
					}
				} else if (s2[0].equals("Cheese")) {
					if (s2[1].equals("x:")) {
						cheeseX = Integer.parseInt(s2[2]);
					} else if (s2[1].equals("y:")) {
						cheeseY = Integer.parseInt(s2[2]);
					} else if (s2[1].equals("width:")) {
						cheeseWidth = Integer.parseInt(s2[2]);
					} else if (s2[1].equals("height:")) {
						cheeseHeight = Integer.parseInt(s2[2]);
					}
				} else if (s2[0].equals("Time")) {
					timeElapsed = Double.parseDouble(s2[1]);
				} else if (s2[0].equals("Fastest")) {
					fastestTime = Double.parseDouble(s2[1]);
				} else if (s2[0].equals("Difficulty")) {
					difficultyOrdinal = Integer.parseInt(s2[1]);
				} else if (s2[0].equals("Valid:")){
					gameValid = Boolean.parseBoolean(s2[1]);
				}
			}

			// Create the data objects for us to restore.
			Maze maze = new Maze(numRows, numCols, tileWidth);
			int row = 0;
			int col = 0;
			for (int i = 2; i < mazeString.length; i++) {

				if (mazeString[i].equals("1")) {
					maze.openTile(row, col);
				} else {
					maze.closeTile(row, col);
				}
				col++;
				if (col == numCols) {
					col = 0;
					row++;
				}
			}

			Player player = new Player(playerX, playerY, playerHeight,
					playerWidth, playerBorder);
			Cheese cheese = new Cheese(cheeseX, cheeseY, cheeseHeight,
					cheeseWidth);
			RegularMazeDifficulty difficulty = RegularMazeDifficulty.values()[difficultyOrdinal];
			int score = (difficulty.ordinal() + 2)*1000 
					- (int) (1000 * timeElapsed / fastestTime);
			score *= (difficulty.ordinal() + 1);

			mazeGameFields = new RegularMazeGameFields(maze, player, cheese,
					gameValid, timeElapsed, score, fastestTime, difficulty);

			sc.close();
		} catch (Exception e) {
			
		}
		return mazeGameFields;
	}
}
