package pages;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 * Displays the help page for the Slide Ninja Slide maze game
 * 
 * @author Jeffrey
 * 
 */
public class SNSHelp {
	static boolean back;

	/**
	 * Displays the help page and waits for the back button to be pressed
	 * 
	 * @param frame
	 *            the frame where the page will be displayed
	 */
	public static void show(JFrame frame) {
		initialisePage(frame);

		// wait until a button is clicked
		while (back == false) {
			// wait for other listeners to change the choice field
			Thread.yield();
		}
	}

	private static void initialisePage(JFrame frame) {
		frame.getContentPane().removeAll();

		GridBagConstraints c = null;
		back = false;

		// TITLE
		JPanel titlePanel = new JPanel(new GridBagLayout());
		c = new GridBagConstraints();
		JLabel title = new JLabel("<html><div style=\"text-align: center;\">"
				+ "Slide Ninja Slide<br>" + "How To Play");
		title.setFont(new Font("Comic Sans MS", Font.BOLD, 60));
		title.setForeground(Color.ORANGE);
		titlePanel.add(title, c);
		titlePanel.setBorder(new EmptyBorder(20, 0, 0, 0));

		// CONTENT
		JLabel gameExplanation = new JLabel(
				"<html>"
						+ "You are a pair of Ninjas hunting for treasure! "
						+ "You've randomly stumbled upon a spiral shaped ice maze, "
						+ "with a huge pile of gold in the centre. "
						+ "However, it's guarded by politicians! Touching them will "
						+ "leave you as an incapacitated babbling wreck, unable to move. "
						+ "Only another ninja can talk some sense into you, and get you "
						+ "back on your feet!<br><br>"
						+ "<div style=\"text-align: center;\"><i>"
						+ "Slide on ice! Walk on safe tiles!<br>"
						+ "Bounce off walls! Bounce off random blocks on the ice!<br>"
						+ "Dodge enemies! Or they'll kill you!<br>"
						+ "Revive your incapacitated teammate by tagging them!<br>"
						+ "Thrust on ice*! Gain a 1 second speed boost!<br><br></i></div>"
						+ "<font size='3' color='red'><b>*Please note the use of thrust "
						+ "prevents the player from changing direction for the duration "
						+ "of the thrust. <br>Management is not responsible for any panic, "
						+ "injury, loss, taxes, death or any other direct or indirect "
						+ "inconvenience caused from the use of thrust.<br>"
						+ "10 second cooldown. Use at your own risk.</b></font>"
						+ "</html>");

		JLabel player1Controls = new JLabel("<html>"
				+ "<h1><b>Player 1 Controls</b></h1>"
				+ "<b>Movement:</b> WASD<br>" + "<b>Thrust:</b> Space<br>"
				+ "</html>");

		JLabel player2Controls = new JLabel("<html>"
				+ "<h1><b>Player 2 Controls<b></h1>"
				+ "<b>Movement:</b> IJKL or arrow keys<br>"
				+ "<b>Thrust:</b> Enter<br>" + "</html>");

		gameExplanation.setFont(new Font("Arial", Font.PLAIN, 20));
		player1Controls.setFont(new Font("Arial", Font.PLAIN, 20));
		player2Controls.setFont(new Font("Arial", Font.PLAIN, 20));
		gameExplanation.setForeground(Color.ORANGE);
		player1Controls.setForeground(Color.ORANGE);
		player2Controls.setForeground(Color.ORANGE);
		JPanel contentPanel = new JPanel(new GridBagLayout());
		contentPanel.setBackground(frame.getContentPane().getBackground());

		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.PAGE_START;
		c.insets = new Insets(0, 200, 0, 200);
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		contentPanel.add(gameExplanation, c);

		c = new GridBagConstraints();
		c.insets = new Insets(20, 300, 0, 0);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = 0.5;
		c.gridy = 1;
		c.gridx = 0;
		contentPanel.add(player1Controls, c);

		c = new GridBagConstraints();
		c.insets = new Insets(20, 0, 0, 300);
		c.anchor = GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.weightx = 0.5;
		c.gridy = 1;
		c.gridx = 1;
		contentPanel.add(player2Controls, c);

		// BUTTONS
		JPanel buttonPanel = new JPanel(new GridBagLayout());

		// BACK
		JButton backButton = new JButton("Back");
		backButton.setFont(new Font("Arial", Font.PLAIN, 40));
		backButton.setPreferredSize(new Dimension(200, 100));
		backButton.setFocusPainted(false);
		c = new GridBagConstraints();

		ActionListener backButtonClick = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				back = true;
			}
		};
		backButton.addActionListener(backButtonClick);
		buttonPanel.add(backButton, c);
		buttonPanel.setBorder(new EmptyBorder(0, 0, 20, 0));

		titlePanel.setBackground(null);
		buttonPanel.setBackground(null);

		frame.add(titlePanel, BorderLayout.NORTH);
		frame.add(contentPanel, BorderLayout.CENTER);
		frame.add(buttonPanel, BorderLayout.SOUTH);
		frame.setPreferredSize(new Dimension(1200 + frame.getInsets().left
				+ frame.getInsets().right, 900 + frame.getInsets().bottom
				+ frame.getInsets().top));
		frame.pack();
	}
}
