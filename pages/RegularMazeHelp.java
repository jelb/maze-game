package pages;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 * Displays the help page for the normal maze game
 * 
 * @author Jeffrey
 * 
 */
public class RegularMazeHelp {
	private static boolean back;

	/**
	 * Displays the help page and waits for the back button to be pressed
	 * 
	 * @param frame
	 *            the frame where the page will be displayed
	 */
	public static void show(JFrame frame) {
		initialisePage(frame);

		// wait until a button is clicked
		while (back == false) {
			// wait for other listeners to change the choice field
			Thread.yield();
		}
	}

	private static void initialisePage(JFrame frame) {
		frame.getContentPane().removeAll();

		GridBagConstraints c = null;
		back = false;

		// TITLE
		JPanel titlePanel = new JPanel(new GridBagLayout());
		c = new GridBagConstraints();
		JLabel title = new JLabel("<html><div style=\"text-align: center;\">"
				+ "Regular Maze<br>" + "How To Play");
		title.setFont(new Font("Comic Sans MS", Font.BOLD, 60));
		title.setForeground(Color.ORANGE);
		titlePanel.add(title, c);
		titlePanel.setBorder(new EmptyBorder(20, 0, 0, 0));

		// CONTENT
		JLabel gameExplanation = new JLabel("<html>"
					+ "You are a hungry test mouse being studied for memory and <br>"
					+ "cognitive thinking research. <br>"
					+ "Your goal is to locate and reach the cheese strategically <br>"
					+ "placed at the opposing end of the maze." + "</div></html>");

		JLabel playerControls = new JLabel("<html>"
				+ "<h1><b>Player Controls</b></h1>"
				+ "<b>Movement:</b> WASD or Arrow Keys<br>"
				+ "<b>Change speed:</b> 1 2 3<br>"
				+ "<b>Toggle butter(sliding mode):</b> Space<br>"
				+ "<b>Show hint:</b> H<br>" + "</html>");

		gameExplanation.setFont(new Font("Arial", Font.PLAIN, 20));
		playerControls.setFont(new Font("Arial", Font.PLAIN, 20));
		gameExplanation.setForeground(Color.ORANGE);
		playerControls.setForeground(Color.ORANGE);
		JPanel contentPanel = new JPanel(new GridBagLayout());
		contentPanel.setBackground(frame.getContentPane().getBackground());

		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.PAGE_START;
		c.insets = new Insets(0, 200, 0, 200);
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		contentPanel.add(gameExplanation, c);

		c = new GridBagConstraints();
		c.insets = new Insets(70, 200, 0, 200);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.CENTER;
		c.gridy = 1;
		c.gridx = 0;
		contentPanel.add(playerControls, c);

		// BUTTONS
		JPanel buttonPanel = new JPanel(new GridBagLayout());

		// BACK
		JButton backButton = new JButton("Back");
		backButton.setFont(new Font("Arial", Font.PLAIN, 40));
		backButton.setPreferredSize(new Dimension(200, 100));
		backButton.setFocusPainted(false);
		c = new GridBagConstraints();

		ActionListener backButtonClick = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				back = true;
			}
		};
		backButton.addActionListener(backButtonClick);
		buttonPanel.add(backButton, c);
		buttonPanel.setBorder(new EmptyBorder(0, 0, 20, 0));

		titlePanel.setBackground(null);
		buttonPanel.setBackground(null);

		frame.add(titlePanel, BorderLayout.NORTH);
		frame.add(contentPanel, BorderLayout.CENTER);
		frame.add(buttonPanel, BorderLayout.SOUTH);
		frame.setPreferredSize(new Dimension(1200 + frame.getInsets().left
				+ frame.getInsets().right, 900 + frame.getInsets().bottom
				+ frame.getInsets().top));
		frame.pack();
	}
}
