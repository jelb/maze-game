package pages;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import enums.Choice;

/**
 * This is the page that contains the menu in which you either choose to Start a
 * new game, Load a previous one, or Quit the game entirely
 * 
 * @author Jeffrey
 * 
 */
public class MainMenu {
	private static Choice choice;

	/**
	 * Displays the page and determines the actions when a button is pressed
	 * 
	 * @param frame
	 *            the frame on which the page is displayed
	 * @return the users choice as to what they want to do(i.e. Play, Load, or
	 *         Quit)
	 */
	public static Choice show(JFrame frame) {
		initialisePage(frame);

		// wait until a button is clicked
		while (choice == null) {
			// wait for other listeners to change the choice field
			Thread.yield();
		}
		return choice;
	}

	private static void initialisePage(JFrame frame) {
		frame.getContentPane().removeAll();
		for (KeyListener k : frame.getKeyListeners()) {
			frame.removeKeyListener(k);
		}

		GridBagConstraints c = null;
		choice = null;

		// TITLE
		JPanel titlePanel = new JPanel(new GridBagLayout());
		c = new GridBagConstraints();
		JLabel title = new JLabel("Maze Game Title Goes Here");
		title.setFont(new Font("Comic Sans MS", Font.BOLD, 60));
		title.setForeground(Color.ORANGE);
		titlePanel.add(title, c);
		titlePanel.setBorder(new EmptyBorder(20, 0, 0, 0));

		// BUTTONS
		JPanel buttonPanel = new JPanel(new GridBagLayout());

		// PLAY
		JButton playButton = new JButton("Play");
		playButton.setFont(new Font("Arial", Font.PLAIN, 40));
		playButton.setPreferredSize(new Dimension(200, 100));
		playButton.setFocusPainted(false);
		c = new GridBagConstraints();
		c.gridy = 0;
		c.gridx = 0;

		ActionListener playButtonClick = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				choice = Choice.PLAY;
			}
		};
		playButton.addActionListener(playButtonClick);
		buttonPanel.add(playButton, c);

		// LOAD
		JButton loadButton = new JButton("Load");
		loadButton.setFont(new Font("Arial", Font.PLAIN, 40));
		loadButton.setPreferredSize(new Dimension(200, 100));
		loadButton.setFocusPainted(false);
		c.gridy = 1;
		c.insets = new Insets(100, 0, 0, 0);

		ActionListener loadButtonClick = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				choice = Choice.LOAD;
			}
		};
		loadButton.addActionListener(loadButtonClick);
		buttonPanel.add(loadButton, c);

		// QUIT
		JButton quitButton = new JButton("Quit");
		quitButton.setFont(new Font("Arial", Font.PLAIN, 40));
		quitButton.setPreferredSize(new Dimension(200, 100));
		quitButton.setFocusPainted(false);
		c.gridx = 0;
		c.gridy = 2;

		ActionListener quitButtonClick = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				choice = Choice.QUIT;
			}
		};
		quitButton.addActionListener(quitButtonClick);
		buttonPanel.add(quitButton, c);

		titlePanel.setBackground(null);
		buttonPanel.setBackground(null);
		frame.add(titlePanel, BorderLayout.NORTH);
		frame.add(buttonPanel, BorderLayout.CENTER);
		frame.setPreferredSize(new Dimension(1200 + frame.getInsets().left
				+ frame.getInsets().right, 900 + frame.getInsets().bottom
				+ frame.getInsets().top));
		frame.pack();
		frame.setVisible(true);
	}
}
