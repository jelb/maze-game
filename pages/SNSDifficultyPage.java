package pages;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import slideNinjaSlide.PlaySlideNinjaSlide;
import enums.SNSDifficulty;

/**
 * This is the page where you choose which difficulty you would like to play
 * Slide Ninja Slide on.
 * 
 * @author Jeffrey
 * 
 */
public class SNSDifficultyPage {
	private static SNSDifficulty difficulty;
	private static boolean back;

	/***
	 * Displays the page and determines the difficulty of the maze when a button
	 * is pressed
	 * 
	 * @param frame
	 *            the frame where the page will be displayed
	 * @return false if they want to return to game menu, true if otherwise
	 */
	public static boolean show(JFrame frame) {
		initialisePage(frame);

		// wait until a button is clicked
		while (back == false) {
			if (difficulty != null) {
				PlaySlideNinjaSlide.play(frame, difficulty);
				difficulty = null;
				return true; // return true, we played
			}
			// wait for other listeners to change the choice field
			Thread.yield();
		}
		return false; // return false, we pressed the back button
	}

	private static void initialisePage(JFrame frame) {
		frame.getContentPane().removeAll();

		GridBagConstraints c = null;
		difficulty = null;
		back = false;

		// TITLE
		JPanel titlePanel = new JPanel(new GridBagLayout());
		c = new GridBagConstraints();
		JLabel title = new JLabel("Choose your difficulty");
		title.setFont(new Font("Comic Sans MS", Font.BOLD, 60));
		title.setForeground(Color.ORANGE);
		titlePanel.add(title, c);
		titlePanel.setBorder(new EmptyBorder(20, 0, 0, 0));

		// DIFFICULTIES
		JPanel difficultiesPanel = new JPanel(new GridBagLayout());

		// EASY
		JButton easyButton = new JButton("Easy");
		easyButton.setFont(new Font("Arial", Font.PLAIN, 40));
		easyButton.setPreferredSize(new Dimension(200, 100));
		easyButton.setFocusPainted(false);
		c = new GridBagConstraints();
		c.gridy = 0;
		c.ipadx = 40;

		ActionListener easyButtonClick = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				difficulty = SNSDifficulty.EASY;
			}
		};
		easyButton.addActionListener(easyButtonClick);
		difficultiesPanel.add(easyButton, c);

		// MEDIUM
		JButton mediumButton = new JButton("Medium");
		mediumButton.setFont(new Font("Arial", Font.PLAIN, 40));
		mediumButton.setPreferredSize(new Dimension(200, 100));
		mediumButton.setFocusPainted(false);
		c.gridy = 1;
		c.insets = new Insets(100, 0, 0, 0);

		ActionListener mediumButtonClick = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				difficulty = SNSDifficulty.MEDIUM;
			}
		};
		mediumButton.addActionListener(mediumButtonClick);
		difficultiesPanel.add(mediumButton, c);

		// HARD
		JButton hardButton = new JButton("Hard");
		hardButton.setFont(new Font("Arial", Font.PLAIN, 40));
		hardButton.setPreferredSize(new Dimension(200, 100));
		hardButton.setFocusPainted(false);
		c.gridy = 2;

		ActionListener hardButtonClick = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				difficulty = SNSDifficulty.HARD;
			}
		};
		hardButton.addActionListener(hardButtonClick);
		difficultiesPanel.add(hardButton, c);

		// BACK BUTTON PANEL
		JPanel backPanel = new JPanel(new GridBagLayout());
		JButton backButton = new JButton("Back");
		backButton.setFont(new Font("Arial", Font.PLAIN, 40));
		backButton.setPreferredSize(new Dimension(200, 100));
		backButton.setFocusPainted(false);
		c = new GridBagConstraints();

		ActionListener backButtonClick = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				back = true;
			}
		};
		backButton.addActionListener(backButtonClick);
		backPanel.add(backButton, c);
		backPanel.setBorder(new EmptyBorder(0, 0, 20, 0));
		backPanel.setBackground(null);

		titlePanel.setBackground(null);
		difficultiesPanel.setBackground(null);
		frame.add(titlePanel, BorderLayout.NORTH);
		frame.add(difficultiesPanel, BorderLayout.CENTER);
		frame.add(backPanel, BorderLayout.SOUTH);
		frame.setPreferredSize(new Dimension(1200 + frame.getInsets().left
				+ frame.getInsets().right, 900 + frame.getInsets().bottom
				+ frame.getInsets().top));
		frame.pack();
	}

}
