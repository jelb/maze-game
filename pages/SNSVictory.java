package pages;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.text.DecimalFormat;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 * This is the game over page of the SNS game if the player has won. Displays
 * amount of deaths and time elapsed
 * 
 * @author Jeffrey
 * 
 */
public class SNSVictory {
	private static boolean finish;

	/**
	 * Displays the victory page and waits for the Main menu button to be
	 * pressed
	 * 
	 * @param frame
	 *            the frame on which the page will be displayed
	 * @param p1Deaths
	 *            the amount of times player 1 has died
	 * @param p2Deaths
	 *            the amount of times player 2 has died
	 * @param timeElapsed
	 *            the amount of time that has elapsed since starting
	 */
	public static void show(JFrame frame, int p1Deaths, int p2Deaths,
			double timeElapsed) {
		initialisePage(frame, p1Deaths, p2Deaths, timeElapsed);
		while (finish == false) {
			// wait for other listeners to change the choice field
			Thread.yield();
		}
	}

	private static void initialisePage(JFrame frame, int p1Deaths,
			int p2Deaths, double timeElapsed) {
		for (KeyListener k : frame.getKeyListeners()) {
			frame.removeKeyListener(k);
		}
		;
		frame.getContentPane().removeAll();
		frame.getContentPane().setBackground(new Color(20, 50, 100));
		finish = false;
		String time = new DecimalFormat("#.##").format(timeElapsed);

		// TITLE
		JPanel titlePanel = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		JLabel title = new JLabel("You are both Ninjas!");
		title.setFont(new Font("Comic Sans MS", Font.BOLD, 60));
		title.setForeground(Color.ORANGE);
		titlePanel.add(title, c);
		titlePanel.setBorder(new EmptyBorder(20, 0, 0, 0));
		titlePanel.setBackground(null);

		// CONTENT
		JPanel contentPanel = new JPanel(new GridBagLayout());
		JLabel analysis = new JLabel(
				"<html><div style=\"text-align: center;\">"
						+ "You took <b>"
						+ time
						+ "</b> seconds!<br>"
						+ "Player 1 died <b><font color='red'>"
						+ p1Deaths
						+ "</font></b> times!<br>"
						+ "Player 2 died <b><font color='red'>"
						+ p2Deaths
						+ "</font></b> times!<br>"
						+ "</div><br><br><br>"
						+ "You both teleport out of the maze in search of more "
						+ "treasure. <br> Neither of you are really sure why you didn't "
						+ "teleport to the treasure in the first place. <br>"
						+ " All of that ice skating was really tiring.<br>"
						+ "</html>");

		analysis.setFont(new Font("Arial", Font.PLAIN, 20));
		analysis.setForeground(Color.ORANGE);
		c = new GridBagConstraints();
		c.insets = new Insets(0, 0, 0, 0);
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.fill = GridBagConstraints.BOTH;
		contentPanel.add(analysis, c);
		contentPanel.setBackground(null);
		contentPanel.setPreferredSize(new Dimension(1000, 500));

		// MAIN MENU BUTTON
		JPanel buttonPanel = new JPanel(new GridBagLayout());
		JButton menuButton = new JButton("Main Menu");
		menuButton.setFont(new Font("Arial", Font.PLAIN, 40));
		menuButton.setPreferredSize(new Dimension(400, 100));
		menuButton.setFocusPainted(false);
		c = new GridBagConstraints();

		ActionListener menuButtonClick = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				finish = true;
			}
		};
		menuButton.addActionListener(menuButtonClick);
		buttonPanel.add(menuButton, c);
		buttonPanel.setBorder(new EmptyBorder(0, 0, 60, 0));
		buttonPanel.setBackground(null);

		frame.add(titlePanel, BorderLayout.NORTH);
		frame.add(contentPanel, BorderLayout.CENTER);
		frame.add(buttonPanel, BorderLayout.SOUTH);
		frame.setPreferredSize(new Dimension(1200 + frame.getInsets().left
				+ frame.getInsets().right, 900 + frame.getInsets().bottom
				+ frame.getInsets().top));
		frame.pack();
	}
}
