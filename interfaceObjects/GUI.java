package interfaceObjects;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class GUI extends JPanel {
	private static final long serialVersionUID = 1L;
	private JLabel timeText;
	private JLabel timeNumber;
	private JLabel scoreText;
	private JLabel scoreNumber;
	private JLabel validLabel;
	private boolean gameValid;
	private String gameValidString;
	private JButton saveButton;
	private JButton hintButton;
	private JLabel hintCooldownLabel;
	private int hintCooldown;
	private String hintCooldownString;
	private JButton quitButton;

	public GUI(double startingTime, int startingScore, ActionListener 
			hintButtonClick, ActionListener saveButtonClick, ActionListener 
			quitButtonClick, boolean gameValid) {
		super(new BorderLayout());
		setBackground(new Color(20, 50, 100));
		setPreferredSize(new Dimension(300, 900));
		
		this.gameValid = gameValid;
		updateGameValidString();
		
		JPanel topPanel = new JPanel(new GridBagLayout());
		// TIME TEXT
		timeText = new JLabel(String.format("Time"));
		timeText.setFont(new Font("Arial", Font.PLAIN, 30));
		timeText.setForeground(new Color(255,215,0));
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.VERTICAL;
		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(100,0,0,20);
		topPanel.add(timeText, c);
		
		// TIME NUMBER
		timeNumber = new JLabel(String.format("%1$.2f", startingTime),
				JLabel.CENTER);
		timeNumber.setFont(new Font("Arial", Font.BOLD, 25));
		timeNumber.setBackground(Color.BLACK);
		timeNumber.setForeground(Color.ORANGE);
		timeNumber.setPreferredSize(new Dimension(80, 30));
		timeNumber.setOpaque(true);
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.VERTICAL;
		c.gridx = 0;
		c.gridy = 1;
		c.insets = new Insets(0,0,0,20);
		topPanel.add(timeNumber, c);
		
		// SCORE TEXT
		scoreText = new JLabel(String.format("Score"));
		scoreText.setFont(new Font("Arial", Font.PLAIN, 30));
		scoreText.setForeground(new Color(255,215,0));
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.VERTICAL;
		c.gridx = 1;
		c.gridy = 0;
		c.insets = new Insets(100,20,0,0);
		topPanel.add(scoreText, c);
		
		// SCORE NUMBER
		scoreNumber = new JLabel(String.format("%1$d", startingScore),
				JLabel.CENTER);
		scoreNumber.setFont(new Font("Arial", Font.BOLD, 25));
		scoreNumber.setBackground(Color.BLACK);
		scoreNumber.setForeground(Color.ORANGE);
		scoreNumber.setPreferredSize(new Dimension(80, 30));
		scoreNumber.setOpaque(true);
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.VERTICAL;
		c.gridx = 1;
		c.gridy = 1;
		c.insets = new Insets(0,20,0,0);
		topPanel.add(scoreNumber, c);
		
		// VALID LABEL
		validLabel = new JLabel(gameValidString);
		validLabel.setFont(new Font("Arial", Font.PLAIN, 30));
		validLabel.setForeground(new Color(255,215,0));
		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 2;
		c.insets = new Insets(50,0,0,0);
		c.gridwidth = 2;
		topPanel.add(validLabel, c);
		topPanel.setOpaque(false);
		
		
		JPanel centrePanel = new JPanel(new GridBagLayout());
		// HINT BUTTON
		hintButton = new JButton("HINT");
		hintButton.setBackground(new Color(0, 191, 255));
		hintButton.setBorder(BorderFactory.createMatteBorder(
                3, 3, 4, 3, new Color(30, 144, 255)));
		hintButton.setFocusPainted(false);
		hintButton.setFocusable(false);
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.CENTER;
		c.gridx = 0;
		c.gridy = 3;
		c.ipady = 30;
		c.ipadx = 30;
		c.gridwidth = 1;
		hintButton.addActionListener(hintButtonClick);
		centrePanel.add(hintButton, c);
		
		// HINT COOLDOWN
		hintCooldownLabel = new JLabel("Ready!", JLabel.CENTER);
		hintCooldownLabel.setFont(new Font("Arial", Font.BOLD, 25));
		hintCooldownLabel.setBackground(Color.BLACK);
		hintCooldownLabel.setForeground(Color.ORANGE);
		hintCooldownLabel.setPreferredSize(new Dimension(90, 30));
		hintCooldownLabel.setOpaque(true);
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.CENTER;
		c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 3;
		c.insets = new Insets(0,15,0,0);
		centrePanel.add(hintCooldownLabel, c);
		centrePanel.setOpaque(false);
		
		
		// SAVE AND QUIT
		JPanel bottomPanel = new JPanel(new GridBagLayout());
		saveButton = new JButton("SAVE");
		saveButton.setFocusPainted(false);
		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.ipady = 30;
		c.ipadx = 30;
		c.insets = new Insets(30,0,30,15);
		saveButton.addActionListener(saveButtonClick);
		saveButton.setFocusable(false);
		bottomPanel.add(saveButton, c);
		
		quitButton = new JButton("QUIT");
		quitButton.addActionListener(quitButtonClick);
		c.gridy = 0;
		c.gridx = 1;
		c.insets = new Insets(30,15,30,0);
		quitButton.setFocusable(false);
		bottomPanel.add(quitButton, c);
		bottomPanel.setOpaque(false);
		
		add(topPanel, BorderLayout.NORTH);
		add(centrePanel, BorderLayout.CENTER);
		add(bottomPanel, BorderLayout.SOUTH);

		setVisible(true);
	}

	private void updateHintCooldown(int newCooldown) {
		hintCooldown = newCooldown;
		if (hintCooldown == 0 || hintCooldown == 11) {
			hintCooldownString = "<html><div style=\"text-align: center;\">"
					+ "Ready!"
					+ "</div></html>";
		} else {
			hintCooldownString = "<html><div style=\"text-align: center;\">"
					+ Integer.toString(hintCooldown) 
					+ "</div></html>";
		}
		hintCooldownLabel.setText(hintCooldownString);
		if (hintCooldown == 0 || hintCooldown == 11) {
			hintCooldownLabel.setForeground(Color.GREEN);
		} else {
			hintCooldownLabel.setForeground(Color.RED);
		}
	}

	public void update(double newTime, int newScore, boolean gameValid,
			int hintCooldown) {
		timeNumber.setText(String.format("%1$.1f", newTime));
		scoreNumber.setText(String.format("%1$d", newScore));
		if (this.gameValid != gameValid) {
			this.gameValid = gameValid;
			updateGameValidString();
			validLabel.setText(gameValidString);
		}
		updateHintCooldown(hintCooldown);
	}
	
	private void updateGameValidString() {
		if (gameValid) {
			gameValidString = "<html><div style=\"text-align: center;\">"
					+ "This game is<br><font color='green'><b>valid </b></font>"
					+ "</div></html>";
		} else {
			gameValidString = "<html><div style=\"text-align: center;\">"
					+ "This game is<br><font color='red'><b>NOT valid</b></font>"
					+ "</div></html>";
		}
	}
}
