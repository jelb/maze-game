package interfaceObjects;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class SNSGui extends JPanel {
	private static final long serialVersionUID = 1L;
	private JLabel timeText;
	private JLabel timeNumber;
	private JLabel p1DeathsText;
	private JLabel p1DeathCountLabel;
	private int p1DeathCount;
	private JLabel p2DeathsText;
	private JLabel p2DeathCountLabel;
	private int p2DeathCount;
	private JLabel p1ThrustText;
	private JLabel p1ThrustCooldownLabel;
	private int p1ThrustCooldown;
	private String p1ThrustCooldownString;
	private JLabel p2ThrustText;
	private JLabel p2ThrustCooldownLabel;
	private String p2ThrustCooldownString;
	private int p2ThrustCooldown;
	private JButton quitButton;

	public SNSGui(ActionListener quitButtonClick) {
		super(new BorderLayout());
		setBackground(new Color(20, 50, 100));
		setPreferredSize(new Dimension(284, 900)); // 300 - 16
												// the wall kind of sticks out
												// into gui space
		
		p1ThrustCooldown = 0;
		p2ThrustCooldown = 0;
		p1DeathCount = 0;
		p2DeathCount = 0;
		
		JPanel topPanel = new JPanel(new GridBagLayout());
		// TIME TEXT
		timeText = new JLabel(String.format("Time"));
		timeText.setFont(new Font("Arial", Font.PLAIN, 30));
		timeText.setForeground(new Color(255,215,0));
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.CENTER;
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(20,0,0,0);
		topPanel.add(timeText, c);
		
		// TIME NUMBER
		timeNumber = new JLabel("0", JLabel.CENTER);
		timeNumber.setFont(new Font("Arial", Font.BOLD, 25));
		timeNumber.setBackground(Color.BLACK);
		timeNumber.setForeground(Color.ORANGE);
		timeNumber.setPreferredSize(new Dimension(80, 30));
		timeNumber.setOpaque(true);
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.CENTER;
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 1;
		c.insets = new Insets(0,0,0,0);
		topPanel.add(timeNumber, c);
		
		// P1DEATHS_TEXT
		p1DeathsText = new JLabel(String.format("<html>"
				+ "<div style=\"text-align: center;\">"
				+ "Player 1 <br>Death Count"
				+ "</div></html>"));
		p1DeathsText.setFont(new Font("Arial", Font.PLAIN, 20));
		p1DeathsText.setForeground(new Color(255,215,0));
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.WEST;
		c.gridx = 0;
		c.gridy = 2;
		c.insets = new Insets(100, 0, 0, 15);
		topPanel.add(p1DeathsText, c);
		
		// P1DEATHS_NUMBER
		p1DeathCountLabel = new JLabel("0", JLabel.CENTER);
		p1DeathCountLabel.setFont(new Font("Arial", Font.BOLD, 15));
		p1DeathCountLabel.setBackground(Color.BLACK);
		p1DeathCountLabel.setForeground(Color.GREEN);
		p1DeathCountLabel.setPreferredSize(new Dimension(80, 30));
		p1DeathCountLabel.setOpaque(true);
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.CENTER;
		c.gridx = 0;
		c.gridy = 3;
		c.insets = new Insets(0, 0, 0, 15);
		topPanel.add(p1DeathCountLabel, c);
		
		// P2DEATHS_TEXT
		p2DeathsText = new JLabel(String.format("<html>"
				+ "<div style=\"text-align: center;\">"
				+ "Player 2 <br>Death Count"
				+ "</div></html>"));
		p2DeathsText.setFont(new Font("Arial", Font.PLAIN, 20));
		p2DeathsText.setForeground(new Color(255, 215, 0));
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.EAST;
		c.gridx = 1;
		c.gridy = 2;
		c.insets = new Insets(100, 15, 0, 0);
		topPanel.add(p2DeathsText, c);
		
		// P2DEATHS_NUMBER
		p2DeathCountLabel = new JLabel("0", JLabel.CENTER);
		p2DeathCountLabel.setFont(new Font("Arial", Font.BOLD, 15));
		p2DeathCountLabel.setBackground(Color.BLACK);
		p2DeathCountLabel.setForeground(Color.GREEN);
		p2DeathCountLabel.setPreferredSize(new Dimension(80, 30));
		p2DeathCountLabel.setOpaque(true);
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.CENTER;
		c.gridx = 1;
		c.gridy = 3;
		c.insets = new Insets(0, 15, 0, 0);
		topPanel.setOpaque(false);
		topPanel.add(p2DeathCountLabel, c);
		
		
		JPanel centrePanel = new JPanel(new GridBagLayout());
		// P1THRUST_TEXT
		p1ThrustText = new JLabel(String.format("<html>"
				+ "<div style=\"text-align: center;\">"
				+ "Player 1 <br>Thrust"
				+ "</div></html>"));
		p1ThrustText.setFont(new Font("Arial", Font.PLAIN, 30));
		p1ThrustText.setForeground(new Color(255,215,0));
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.CENTER;
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		c.insets = new Insets(0, 0, 0, 15);
		centrePanel.add(p1ThrustText, c);
		
		// P1THRUST_COOLDOWN
		p1ThrustCooldownLabel = new JLabel("Ready!", JLabel.CENTER);
		p1ThrustCooldownLabel.setFont(new Font("Arial", Font.BOLD, 25));
		p1ThrustCooldownLabel.setBackground(Color.BLACK);
		p1ThrustCooldownLabel.setForeground(Color.ORANGE);
		p1ThrustCooldownLabel.setPreferredSize(new Dimension(90, 30));
		p1ThrustCooldownLabel.setOpaque(true);
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.CENTER;
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 1;
		c.insets = new Insets(0, 0, 0, 15);
		centrePanel.add(p1ThrustCooldownLabel, c);
		centrePanel.setOpaque(false);
		
		// P2THRUST_TEXT
		p2ThrustText = new JLabel(String.format("<html>"
				+ "<div style=\"text-align: center;\">"
				+ "Player 2 <br>Thrust"
				+ "</div></html>"));
		p2ThrustText.setFont(new Font("Arial", Font.PLAIN, 30));
		p2ThrustText.setForeground(new Color(255,215,0));
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.CENTER;
		c.gridx = 1;
		c.gridy = 0;
		c.gridwidth = 1;
		c.insets = new Insets(0, 15, 0, 0);
		centrePanel.add(p2ThrustText, c);
		
		// P2THRUST_COOLDOWN
		p2ThrustCooldownLabel = new JLabel("Ready!", JLabel.CENTER);
		p2ThrustCooldownLabel.setFont(new Font("Arial", Font.BOLD, 25));
		p2ThrustCooldownLabel.setBackground(Color.BLACK);
		p2ThrustCooldownLabel.setForeground(Color.ORANGE);
		p2ThrustCooldownLabel.setPreferredSize(new Dimension(90, 30));
		p2ThrustCooldownLabel.setOpaque(true);
		c = new GridBagConstraints();
		c.anchor = GridBagConstraints.CENTER;
		c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 1;
		c.insets = new Insets(0, 15, 0, 0);
		centrePanel.add(p2ThrustCooldownLabel, c);
		centrePanel.setOpaque(false);
		
		// BOTTOM PANEL
		// QUIT
		JPanel bottomPanel = new JPanel(new GridBagLayout());
		
		quitButton = new JButton("QUIT");
		quitButton.addActionListener(quitButtonClick);
		c.gridy = 0;
		c.gridx = 1;
		c.insets = new Insets(30,15,30,0);
		bottomPanel.add(quitButton, c);
		bottomPanel.setOpaque(false);
		
		add(topPanel, BorderLayout.NORTH);
		add(centrePanel, BorderLayout.CENTER);
		add(bottomPanel, BorderLayout.SOUTH);

		setVisible(true);
	}

	public void update(double newTime, int p1ThrustCD, int p2ThrustCD,
			int p1Deaths, int p2Deaths) {
		timeNumber.setText(String.format("%1$.1f", newTime));
		updateCooldown(p1ThrustCD, p2ThrustCD);
		updateDeaths(p1Deaths, p2Deaths);
	}
	
	private void updateDeaths(int p1Deaths, int p2Deaths) {
		p1DeathCount = p1Deaths;
		p2DeathCount = p2Deaths;
		p1DeathCountLabel.setText(Integer.toString(p1DeathCount));
		p2DeathCountLabel.setText(Integer.toString(p2DeathCount));
		if (p1DeathCount > 0) {
			p1DeathCountLabel.setForeground(Color.RED);
		}
		if (p2DeathCount > 0) {
			p2DeathCountLabel.setForeground(Color.RED);
		}
	}
	
	private void updateCooldown(int p1New, int p2New) {
		p1ThrustCooldown = p1New;
		p2ThrustCooldown = p2New;
		
		// update player 1
		if (p1ThrustCooldown == 0 || p1ThrustCooldown == 11) {
			p1ThrustCooldownString = "<html><div style=\"text-align: center;\">"
					+ "Ready!"
					+ "</div></html>";
		} else {
			p1ThrustCooldownString = "<html><div style=\"text-align: center;\">"
					+ Integer.toString(p1ThrustCooldown) 
					+ "</div></html>";
		}
		p1ThrustCooldownLabel.setText(p1ThrustCooldownString);
		if (p1ThrustCooldown == 0 || p1ThrustCooldown == 11) {
			p1ThrustCooldownLabel.setForeground(Color.GREEN);
		} else {
			p1ThrustCooldownLabel.setForeground(Color.RED);
		}
		
		// update player 2
		if (p2ThrustCooldown == 0 || p2ThrustCooldown == 11) {
			p2ThrustCooldownString = "<html><div style=\"text-align: center;\">"
					+ "Ready!"
					+ "</div></html>";
		} else {
			p2ThrustCooldownString = "<html><div style=\"text-align: center;\">"
					+ Integer.toString(p2ThrustCooldown) 
					+ "</div></html>";
		}
		p2ThrustCooldownLabel.setText(p2ThrustCooldownString);
		if (p2ThrustCooldown == 0 || p2ThrustCooldown == 11) {
			p2ThrustCooldownLabel.setForeground(Color.GREEN);
		} else {
			p2ThrustCooldownLabel.setForeground(Color.RED);
		}
	}
}
