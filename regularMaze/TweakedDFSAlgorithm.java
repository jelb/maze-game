package regularMaze;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Stack;

import enums.Difficulty;
import enums.RegularMazeDifficulty;
import gameObjects.Maze;
import gameObjects.Tile;


/**
 * This Class contains no fields. It simply holds the method that generates
 * a maze.
 * 
 * ==============================================================
 * The method is static. This means that you call it using:
 * MazeGenerator.generate()
 * (similar to Collections.sort())
 * 
 * Non static methods are called by creating an object with this Class and using
 * the object:
 * 
 * MazeGenerator m = new MazeGenerator();
 * m.generate()
 * 
 * The static way is better in this case.
 * ==============================================================
 */
public class TweakedDFSAlgorithm implements MazeAlgorithm {
	/*
	 * Maze generation theory
	 * Imagine that the maze starts like this:
	 * * = border tile
	 * x = closed tile
	 * o = cell tile
	 * 
	 * 		* * * * * * * *
	 * 		* o x o x o x *
	 * 		* x x x x x x *
	 * 		* o x o x o x *
	 * 		* x x x x x x *
	 * 		* o x o x o x *
	 * 		* * * * * * * *
	 * 
	 * Not the best diagram. The idea is that the maze starts off as unconnected
	 * cells. To make the maze, we connect cells by breaking the wall between them.
	 * When we connect a cell, that cell won't be connected again (to prevent loops).
	 */
	/*
	 * Maze difficulty behaviour
	 * 
	 * EASY: 
	 * 	- DFS from start to end, randomly expanding to neighbouring cells. 
	 * 
	 * MEDIUM:
	 * 	- Weighted expansion. The maze will favour cells that are further
	 *    away from the end.
	 *  - Infrequently forced branching (every numRows*2 expansions)
	 *  
	 * HARD:
	 * 	- Maze generation is now in two stages:
	 * 	1. EXPANSION
	 * 		- expand far away from the start, favouring topright/botleft 
	 * 		  (rather than the centre of the maze)
	 * 	2. REGULAR GENERATION
	 * 		- favour cells further away from the end
	 * 	- branching is now very frequent
	 *  - reaching a dead-end now forces branching, rather than backtracking
	 *  
	 *  BRUTAL:
	 *  - Second stage of maze generation has been changed. If you constantly 
	 *    try to avoid the end, all of the dead ends will be pointing towards
	 *    the start. No one is going to follow a path that is going the wrong way.
	 *    
	 *    Introducing, ***THE FRONTIER***. 
	 *    
	 *    When behind the frontier, this path is unlikely to end up being part
	 *    of the solution. Try and expand towards the end, so the player wants 
	 *    to follow this likely wrong path.
	 *    
	 *    When in front of the frontier, the path is likely to be part of the 
	 *    solution. Try and expand away from the end, so the player doesn't
	 *    want to follow this likely correct path.
	 *    
	 *    The frontier makes sure the maze generates from topleft to botright
	 *    evenly. This means the path will be longer, but not necessarily 
	 *    easier.
	 *   
	 *  - branching is now less frequent after the expansion stage. This means
	 *    that it will take longer for players to realise that they are going
	 *    down a wrong path.
	 *    
	 *  - weighted shuffling is now changed to VERY HEAVILY favour tiles that 
	 *    will reduce the overall number of useless dead-ends. 
	 *        
	 *        If a tile is a dead-end, the maze will expand there if it is 
	 *        EXTENDING A PATH to it.
	 *        otherwise, the maze will avoid expanding there because it is 
	 *        CREATING A BRANCH to it. (this branch is useless)
	 *        
	 *        If the degree of the next cell is 1 and we DON'T visit it now, it
	 *        is guaranteed to be a dead end when we visit it later. To reduce
	 *        the number of dead ends, the maze will favour expanding to these 
	 *        degree-1 tiles.
	 * 
	 * MASOCHISM: same as brutal but the maze is very big
	 */
	/*
	 * Preconditions: 
	 * - numRows is odd 
	 * - numCols is odd
	 * - tileWidth is greater than 3
	 */
	public Maze generate(Difficulty difficulty) {
		System.out.println("Generating a maze with difficulty: " + difficulty);
		Maze maze = new Maze(difficulty);
		int numRows = difficulty.getNumRows();
		int numCols = difficulty.getNumCols();
		
		int maxDepth = numRows/8;
		if (difficulty == RegularMazeDifficulty.MEDIUM) {
			maxDepth = numRows*2;
		}
		Tile start = maze.getTile(1, 1);
		Tile end = maze.getTile(numRows - 2, numCols - 2);
		ArrayList<Tile> visited = new ArrayList<Tile>();
		Stack<Tile> stack = new Stack<Tile>();
		HashMap<Tile, Tile> parent = new HashMap<Tile, Tile>();

		// Randomly choose to end the maze going up or left. This prevents the 
		// maze branching at the end tile.
		// 1. open end tile
		maze.openTile(end.getRow(), end.getCol()); 
		ArrayList<Tile> nextTiles = new ArrayList<Tile>(
				maze.getDirectNeighbours(end));
		Collections.shuffle(nextTiles); 
		// 2. open the tile directly up or left to end
		maze.openTile(nextTiles.get(0).getRow(), nextTiles.get(0).getCol());

		// Randomly choose to start the maze going right or down. This prevents 
		// the maze branching at the start tile.
		// 1. open start tile
		maze.openTile(1, 1); 
		nextTiles = new ArrayList<Tile>(maze.getUnvisitedCellNeighbours(start));
		Collections.shuffle(nextTiles);
		// push only ONE cell neighbour onto the stack
		stack.push(nextTiles.get(0)); 
		parent.put(nextTiles.get(0), start);

		Tile curr = null;
		boolean expand = false;
		if (difficulty.isAtLeast(RegularMazeDifficulty.HARD)) {
			expand = true;
		}
		int currDepth = 0;
		int frontier = 0;
		
		while (!stack.isEmpty()) {
			// 1. get next cell to expand to
			curr = stack.pop();
			if (difficulty.isAtLeast(RegularMazeDifficulty.BRUTAL)){ 
				if (curr.getRow() + curr.getCol() > frontier) {
					frontier++;
				} else {
					frontier--;
				}
			}
			if (visited.contains(curr)) {
				continue;
			}
			visited.add(curr);
			// remove all instances of curr from stack
			while(stack.remove(curr)) {}
			
			if (visited.size() == numRows*2) {
				if (difficulty.isAtLeast(RegularMazeDifficulty.BRUTAL)) {
					maxDepth = numRows/2;
				}
				expand = false;
			}
			// 2. connect that cell to it's parent
			connectCell(maze, parent.get(curr), curr);
			
			currDepth++;

			// 3. push on the next cells (current cell's neighbours) onto stack
			// in a random order (using Collections.shuffle())
			nextTiles = new ArrayList<Tile>(maze.getUnvisitedCellNeighbours(curr));
			Collections.shuffle(nextTiles);
			
			if (difficulty.isAtLeast(RegularMazeDifficulty.MEDIUM)) {
				if (expand || curr.getRow() + curr.getCol() < frontier) {
					weightedShuffleExpand(nextTiles, maze, curr, difficulty);
					System.out.println("go forth");
				} else {
					weightedShuffleContract(nextTiles, maze, curr, difficulty);
					System.out.println("come back");
				}
			}
			
			for (Tile t : nextTiles) {
				stack.push(t);
				parent.remove(t);
				parent.put(t, curr);
			}
			
			if (difficulty.isAtLeast(RegularMazeDifficulty.MEDIUM)) { 
				if (currDepth > maxDepth) {
					currDepth = 0;
					newBranch(stack);
				}
			}
			
			if (difficulty.isAtLeast(RegularMazeDifficulty.HARD)) {
				if (nextTiles.isEmpty()) {
					newBranch(stack);
					currDepth = 0;
				}
			}
		}

		printMaze(maze);
		return maze;
	}

	/*
	 * Assigns weights to each tile based on how far they are from the start.
	 * Randomly chooses one of the tiles based on their weight and moves it
	 * to the end of the ArrayList. 
	 * 
	 * This ensures that that tile will be at the top of the stack in the next 
	 * loop iteration. That is, the chosen tile will be expanded to next.
	 * 
	 * The weight is calculated as row^2 + col^2. This encourages expansion towards
	 * the top right and bottom left corners of the maze.
	 * E.g.
	 * Row 10 Col 0  -> weight = 100
	 * Row 5  Col 5  -> weight = 50
	 * Row 0  Col 10 -> weight = 100 
	 * 
	 * The heaviest tile (highest weight) is given an extra chance to be picked.
	 * 
	 * NOTE: special behaviour for dead-end tiles. See comments in method
	 */
	private static void weightedShuffleExpand(ArrayList<Tile> tiles, Maze maze, Tile curr, Difficulty difficulty) {
		// return if the ArrayList is empty
		if (tiles.size() == 0) {
			return;
		}
		
		Tile chosen = null;
		Tile heaviest = null;
		int weightSum = 0;
		int currWeight = 0;
		int maxWeight = -1;
		double extraChance = 0.3; // the heaviest tile will have an extra 30%
								  // chance of being chosen

		HashMap<Tile, Integer> weights = new HashMap<Tile, Integer>();
		
		for (Tile t : tiles) {
			// weight = row^2 + col^2
			currWeight = t.getRow()*t.getRow() + t.getCol()*t.getCol();
			weightSum += currWeight; // update weight sum
			weights.put(t, currWeight); // map tile to its weight
			
			if (difficulty.isAtLeast(RegularMazeDifficulty.BRUTAL)) {
				// if it's a dead end
				if (maze.getCellDegree(t) == 0) { 
					if (maze.getDegree(curr) == 1) {
						// CASE 1: current tile degree is 1
						// we want to expand there, creating a PATH to a dead-end
						currWeight = currWeight*100; 
					} else {
						// CASE 2: current tile degree is > 1
						// we want to avoid it unless there is no other choice, 
						// because it will create a BRANCH to a dead-end
						currWeight = currWeight/100;  
					}
				}
			}
			
			if (currWeight > maxWeight) { // keep track of the "heaviest" tile
				maxWeight = currWeight;
				heaviest = t;
			}
		}
		
		/*
		 * The idea behind selecting the tile is to generate a number in the 
		 * range [0, weightSum]. Then iterate through the tiles, subtracting
		 * that tile's weight from the number generated. Once the number 
		 * is less than or equal to 0, then choose the tile. 
		 *
		 * an alternate way to think of it is to imagine lining up the tiles up
		 * with their CUMULATIVE weights
		 * 
		 *		   tile   :	t1	t2	t3	t4	t5	t6
		 *		   weight : 5	10	3	8	3	1
		 *		cumulative: 5	15	18	26	29	30
		 *
		 * Now generate a number from 0-30. This will correspond to a tile's 
		 * cumulative weight.
		 *
		 * It doesn't stop there. Now we want to give the "heaviest" tile a
		 * higher chance of being selected. So, instead of generating in the 
		 * range [0, weightSum], generate in the range [0, weightSum + k*maxWeight], 
		 * where k is a constant depending on how much of an extra chance you 
		 * want to give.
		 * Apply the same process as before. This time, if you generate a number
		 * higher than weightSum, no tile will be chosen. In that case, choose
		 * the heaviest tile.
		 */

		// get number in range [0, weightSum + extraChance*maxWeight]
		int random = (int) (Math.random()*(weightSum + extraChance*maxWeight + 1));
		
		// see what tile this corresponds to
		for (Tile t : tiles) {
			random -= weights.get(t); // subtract weight from random
			if (random <= 0) { // if random <= 0, choose this tile
				chosen = t;
				break;
			}
		}
		
		if (chosen == null) {  // if no tile was chosen, 
			chosen = heaviest; // choose the heaviest
		}
		
		// move the chosen tile to the end, if it isn't already there
		// this ensures it is at the top of the stack for the next loop iteration
		tiles.remove(chosen);
		tiles.add(chosen);
	}
	
	/*
	 * Assigns weights to each tile based on how far they are from the end tile.
	 * Randomly chooses one of the tiles based on their weight and moves it to 
	 * the end of the ArrayList. 
	 * 
	 * This ensures that that tile will be at the top of the stack in the next 
	 * loop iteration. That is, the chosen tile will be expanded to next.
	 * 
	 * The weight is calculated as (rows from end + cols from end). 
	 * 
	 * The heaviest tile (highest weight) is given an extra chance to be picked.
	 * 
	 * NOTE: special behaviour for dead-end tiles and "forced paths". See 
	 * comments in method
	 * 
	 */
	private static void weightedShuffleContract(ArrayList<Tile> tiles, Maze maze, Tile curr, Difficulty difficulty) {
		// return if the ArrayList is empty
		if (tiles.size() == 0) {
			return;
		}
		
		Tile chosen = null;
		Tile heaviest = null;
		int weightSum = 0;
		int maxWeight = -1;
		int currWeight = 0;
		int endRow = maze.getNumRows() - 2;
		int endCol = maze.getNumCols() - 2;
		double extraChance = 0.9;
		
		HashMap<Tile, Integer> weights = new HashMap<Tile, Integer>();
		
		for (Tile t : tiles) {
			// weight is rows from end + cols from end
			currWeight = (endRow - t.getRow()) + (endCol - t.getCol());
			
			if (difficulty.isAtLeast(RegularMazeDifficulty.BRUTAL)) {
				// if it's a dead end
				if (maze.getCellDegree(t) == 0) { 
					if (maze.getDegree(curr) == 1) {
						// CASE 1: current tile degree is 1
						// we want to expand there, creating a PATH to a dead-end
						currWeight = currWeight*100; 
					} else {
						// CASE 2: current tile degree is > 1
						// we want to avoid it unless there is no other choice, 
						// because it will create a BRANCH to a dead-end
						currWeight = currWeight/100;  
					}
				}
				
				// Try to follow a forced path. This will reduce dead ends later. 
				// If the degree of the next cell is 1 and we DON'T visit it now,
				// it is guaranteed to be a dead end when we visit it later.
				if (maze.getCellDegree(t) == 1) {
					currWeight = currWeight*4;
				}
			}
			
			
			if (currWeight > maxWeight) {
				maxWeight = currWeight;
				heaviest = t;
			}
			weightSum += currWeight;
			weights.put(t, currWeight);
		}
		
		// for an explanation, see weightedShuffleExpand
		// get number in range [0, weightSum + maxWeight]
		int random = (int) (Math.random()*(weightSum + maxWeight*extraChance + 1));
		
		// see what tile this corresponds to, if all the tiles were lined up
		for (Tile t : tiles) {
			random -= weights.get(t);
			if (random <= 0) {
				chosen = t;
				break;
			}
		}
		
		if (chosen == null) {
			chosen = heaviest;
		}
		
		// swap chosen tile to the end, if it isn't already there
		// this ensures it is at the top of the stack for the next loop iteration
		tiles.remove(chosen);
		tiles.add(chosen);
	}
	
	/*
	 * Creates a new branch in the maze. This is done by taking a random element 
	 * and pushing it to the top.
	 */
	private static void newBranch(Stack<Tile> stack) {
		// return if stack is empty
		if (stack.size() == 0) {
			return;
		}
		
		// random number in range [0, (stackSize() - 1)] 
		int random = (int)(Math.random()*((stack.size() - 1))*0.5);
		Tile branch = stack.get(random);
		
		stack.remove(branch);
		stack.push(branch);
	}

	private static void printMaze(Maze maze) {
		int i, j;
		int numRows = maze.getNumRows();
		int numCols = maze.getNumCols();
		for (i = 0; i < numRows; i++) {
			for (j = 0; j < numCols; j++) {
				if (maze.getTile(i, j).isOpen()) {
					System.out.print("  ");
				} else {
					System.out.print("x ");
				}
			}
			System.out.println();
		}
		System.out.println();
	}

	/*
	 * Takes in two tiles. Opens the tile between them. Opens the second tile.
	 * @precondition The tiles must be only 2 units from each other, either 
	 * vertically or horizontally.
	 */
	private static void connectCell(Maze maze, Tile from, Tile to) {
		int row;
		int col;
		if (from.getRow() == to.getRow()) { // if same row
			col = (from.getCol() + to.getCol()) / 2; // average of the 2 cols
			row = from.getRow();
		} else { // same col
			row = (from.getRow() + to.getRow()) / 2; // average of the 2 rows
			col = from.getCol();
		}
		maze.openTile(row, col);
		maze.openTile(to.getRow(), to.getCol());
	}
}