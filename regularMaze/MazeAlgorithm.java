package regularMaze;

import enums.Difficulty;
import gameObjects.Maze;

/**
 * This is the inferface used for the maze Algorithms
 * 
 * @author Jeffrey
 * 
 */
public interface MazeAlgorithm {
	/**
	 * Generates the maze depending on the difficulty chosen. Uses a different
	 * maze generation technique for each difficulty
	 * 
	 * @param difficulty
	 *            the difficulty chosen by the player
	 * @return a maze
	 */
	public Maze generate(Difficulty difficulty);
}
