package regularMaze;
import interfaceObjects.GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.PrintWriter;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;

import pages.Victory;
import playerMovement.RegularMazeMovement;
import enums.Difficulty;
import gameObjects.Cheese;
import gameObjects.GamePanel;
import gameObjects.Maze;
import gameObjects.Player;


/**This is where the regular maze game mode is run from. It listens for and
 * determines player movement.
 * @author Jeffrey
 *
 */
public class PlayRegularMaze {
	// maze objects
	private static Maze maze;
	private static Player player;
	private static Cheese cheese;
	// fields for timer
	private static double timeElapsed;
	private static Timer gameTimer;
	// fields for score
	private static int score;
	private static double fastestTime;
	// fields for hint system
	private static int cooldownLength;
	private static int cooldownElapsed;
	// miscellaneous
	private static Difficulty difficulty;
	private static boolean hintReady;
	private static boolean gameValid;
	private static boolean playing;	
	// player movement
	private static RegularMazeMovement playerMovement;
	
	/**
	 * Play game. If a game was loaded, uses those objects. Otherwise, creates
	 * new ones.
	 * 
	 * @param frame
	 * @param difficulty
	 */
	public static void play (JFrame frame, RegularMazeGameFields fields) {
		initialiseFields(fields); // initialise variables for hint
		frame.getContentPane().removeAll();
		frame.getContentPane().setBackground(Color.WHITE);

		GamePanel gamePanel = new GamePanel();
		gamePanel.addObject(maze);
		gamePanel.addObject(player);
		gamePanel.addObject(cheese);	
		gamePanel.setPreferredSize(new Dimension(900, 900));
		
		frame.add(gamePanel, BorderLayout.CENTER);
		
		ActionListener hint = new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		        tryHint(player, maze);
		    }
		};
		ActionListener save = new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		        save();
		    }
		};
		ActionListener quit = new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		        quit();
		    }
		};
		
		GUI gui = new GUI(timeElapsed, score, hint, save, quit, gameValid);
		frame.add(gui, BorderLayout.EAST);
		frame.pack();
		frame.setPreferredSize(new Dimension(1200 + frame.getInsets().left
				+ frame.getInsets().right, 900 + frame.getInsets().bottom
				+ frame.getInsets().top));
		frame.setResizable(false);
		
		final KeyListener readKey = new KeyListener () {		
			@Override
			public void keyPressed(KeyEvent e) {
				int key = e.getKeyCode();
				if (key == 'W' || key == KeyEvent.VK_UP) {
					playerMovement.up(true);
				}
				if (key == 'A' || key == KeyEvent.VK_LEFT) {
					playerMovement.left(true);
				}
				if (key == 'S' || key == KeyEvent.VK_DOWN) {
					playerMovement.down(true);
				}
				if (key == 'D' || key == KeyEvent.VK_RIGHT) {
					playerMovement.right(true);
				}
				if (key == '1') {
					System.out.println("Speed 1");
					playerMovement.setSpeed(1);
				}
				if (key == '2') {
					System.out.println("Speed 2");
					playerMovement.setSpeed(2);
				}
				if (key == '3') {
					System.out.println("Speed 3");
					playerMovement.setSpeed(3);
					gameValid = false;
				}
				if (key == KeyEvent.VK_SPACE) {
					playerMovement.butter();
				}
				if (key == 'H') {
					tryHint(player, maze);
					gameValid = false;
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				int key = e.getKeyCode();
				if (key == 'W' || key == KeyEvent.VK_UP) {
					playerMovement.up(false);
				}
				if (key == 'A' || key == KeyEvent.VK_LEFT) {
					playerMovement.left(false);
				}
				if (key == 'S' || key == KeyEvent.VK_DOWN) {
					playerMovement.down(false);
				}
				if (key == 'D' || key == KeyEvent.VK_RIGHT) {
					playerMovement.right(false);
				}
			}

			@Override
			public void keyTyped(KeyEvent e) {
			}
		};
		frame.addKeyListener(readKey);
		frame.requestFocus();

		boolean won = false;
		printGameInformation();
		setTimer();
		while (!won && playing) { // while not won, and playing
			gui.update(timeElapsed, score, gameValid, 
					cooldownLength - cooldownElapsed + 1); // hint cooldown
			playerMovement.move();
			frame.repaint();
			if (score > 0) {
				updateScore();
			}
			try {
				Thread.sleep(20); // 50fps
			} catch (InterruptedException e1) {}
			if (player.hitTest(cheese)) {
				won = true;
				System.out.println("All walls passed! You are Awesome!");
			}
		}
		
		stopTimer();
		if (playing) {
			// wait for 0.2 seconds before going to victory screen
			long endTime = System.currentTimeMillis();
			while (System.currentTimeMillis() - endTime < 200) {
				playerMovement.move();
				frame.repaint();
				try {
					Thread.sleep(20); // 50fps
				} catch (InterruptedException e1) {
				}
			}
			Victory.show(frame, gameValid, timeElapsed, score);
		}
	}

	private static void initialiseFields(RegularMazeGameFields fields) {
		// maze objects
		maze = fields.getMaze();
		player = fields.getPlayer();
		cheese = fields.getCheese();
		// fields for timer
		timeElapsed = fields.getTimeElapsed();
		// fields for score
		score = fields.getScore();
		fastestTime = fields.getFastestTime();
		// fields for hint system
		cooldownLength = 10;
		cooldownElapsed = 0;
		// miscellaneous
		difficulty = fields.getDifficulty();
		hintReady = true;
		gameValid = fields.isGameValid();
		playing = true;	
		// player movement
		playerMovement = new RegularMazeMovement(player, maze.getClosedTiles());
	}
	
	private static void printGameInformation() {
		System.out.println("Regular Maze Controls");
		System.out.println("WASD/arrow keys to move");
		System.out.println("1/2/3 to change speed (default 2)");
		System.out.println("space to toggle butter");
		System.out.println("H to show hint (only available every 10s)");
		System.out.println("Warning: using a hint or changing speed invalidates "
							+ "your score!");
	}

	/*
	 * Calculate the fastest time to finish the maze, T.
	 * For every T elapsed, 1000 is taken away.
	 * Easy starts at 2000, so has up to 2*T before score = 0
	 * Medium starts at 3000, so has up to 3*T before score = 0
	 * Brutal starts at 5000, so has up to 5*T before score = 0
	 * 
	 * Also, score is multiplied by the ordinal of the difficulty
	 */
	private static void updateScore() {
		// easy 1, medium 2, hard 3, brutal 4, masochism 5 
		int difficultyScaler = difficulty.ordinal() + 1;
		score = (difficultyScaler+1)*1000 
				- (int) (1000 * timeElapsed / fastestTime);
		score *= difficultyScaler;
		if (score < 0) {
			score = 0;
		}
	}
	
	protected static int getScore() {
		return score;
	}
	
	
	/*
	 * sets the timer to start counting every 0.1s
	 */
	private static void setTimer() {
		gameTimer = new Timer();
		TimerTask gameTimerTick = new TimerTask() {
			@Override
			public void run() {
				timeElapsed += 0.1;
			}
		};
		gameTimer.schedule(gameTimerTick, 0, 100);
	}
	
	private static void stopTimer() {
		gameTimer.cancel();
		gameTimer.purge();
	}
	
	/*
	 * Returns how much time has passed since the timer was started
	 */
	protected static double getGameTimeElapsed() {
		return timeElapsed;
	}
	
	/**
	 * Attempts to hint
	 * if hint is ready,
	 * shows a hint path for 500ms
	 * after that, the hint is slowly cleared
	 * cooldown set for timer for 10s before hintReady is set to true again
	 * if hint not ready, prints cooldown not ready and TODO affects the GUI
	 * 
	 * @param player
	 * @param maze
	 */
	protected static void tryHint(Player player, final Maze maze) {
		if (hintReady) {
			hintReady = false;
			gameValid = false; // FIXED Game valid bug where it is still valid if you click on the jbutton
			System.out.println("Hint requested");
			// pass in the x and y of player's centre
			maze.showHintPath(player.getX() + player.getWidth()/2, 
					player.getY() + player.getHeight()/2);
			
			final Timer timer = new Timer();
			TimerTask removeHint = new TimerTask() {
				@Override
				public void run() {
					while (!maze.allTilesUnhighlighted()) {
						maze.clearHintPath();
						try {
							Thread.sleep(10);
						} catch (InterruptedException e) {}
					}
					timer.cancel();
				}
			};
			timer.schedule(removeHint, 500); // remove hint in 0.5s
			
			cooldownElapsed = 0;
			final Timer cooldownTimer = new Timer();
			TimerTask cooldownTick = new TimerTask() {
				@Override
				public void run() {
					if (cooldownElapsed == cooldownLength) {
						hintReady = true;
						cooldownElapsed++;
						cooldownTimer.cancel();
					} else {
						cooldownElapsed++;
					}
				}
			};
			cooldownTimer.schedule(cooldownTick, 0, 1000);

		} else {
			System.out.println("Hint on cooldown, " + 
					(cooldownLength - cooldownElapsed + 1)
					+ " seconds remaining");
		}
		
	}
	
	/**
	 * Sets the field playing to false, which causes the main method of this
	 * function stop the playing loop and return.
	 */
	protected static void quit() {
		// lazy fix for cooldown timer not cancelling upon quit
		cooldownElapsed = cooldownLength;
		playing = false;
	}
	
	/**
	 * Saves the game's state, then quits the game. 
	 */
	protected static void save() {
		try {
			// Open a file to write to, named SavedObjects.sav.
			PrintWriter saveFile = new PrintWriter("save.sav", "UTF-8");

			// Saving the maze
			saveFile.println("Maze numRows: " + maze.getNumRows());
			saveFile.println("Maze numCols: " + maze.getNumCols());
			saveFile.println("Maze tileWidth: " + maze.getTileWidth());
			saveFile.print("Maze map: ");

			for (int x = 0; x < maze.getNumRows(); x++) {
				for (int y = 0; y < maze.getNumCols(); y++) {
					if (maze.isOpen(x, y)) {
						saveFile.print("1 ");
					} else {
						saveFile.print("0 ");
					}

				}
			}
			saveFile.println();

			// Saving the player
			saveFile.println("Player x: " + player.getX());
			saveFile.println("Player y: " + player.getY());
			saveFile.println("Player width: " + player.getWidth());
			saveFile.println("Player height: " + player.getHeight());
			saveFile.println("Player border: " + player.getDrawBorder());

			// Saving the Cheese
			saveFile.println("Cheese x: " + cheese.getX());
			saveFile.println("Cheese y: " + cheese.getY());
			saveFile.println("Cheese width: " + cheese.getWidth());
			saveFile.println("Cheese height: " + cheese.getHeight());

			// Saving the time elapsed and fastest time
			saveFile.println("Time " + timeElapsed);
			saveFile.println("Fastest " + fastestTime);
			
			// Saving the difficulty
			saveFile.println("Difficulty " + difficulty.ordinal());
			
			saveFile.println("Valid: " + gameValid);
			
			saveFile.close();
		} catch (Exception e) {
		}
		quit();
	}
}
