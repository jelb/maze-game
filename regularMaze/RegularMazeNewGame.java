package regularMaze;

import enums.Difficulty;
import gameObjects.Cheese;
import gameObjects.Maze;
import gameObjects.Player;

import javax.swing.JFrame;

/**Generates a new Maze, creates a new Player and Cheese, adds these to the
 * scene. Adds the scene to the game panel, adds the game panel to the
 * frame.
 * @author Jeffrey
 *
 */
public class RegularMazeNewGame {
	/**
	 * Generates a new Maze, creates a new Player and Cheese, adds these to the
	 * scene. Adds the scene to the game panel, adds the game panel to the
	 * frame. Then calls the game to play
	 * 
	 * @param frame
	 *            The frame on which the game will be loaded
	 * @param difficulty
	 *            the difficulty chosen for the game
	 */
	public static void play(JFrame frame, Difficulty difficulty,
			MazeAlgorithm algo) {
		Maze maze = algo.generate(difficulty);
		Player player = new Player(difficulty, false);
		Cheese cheese = new Cheese(difficulty);
		boolean gameValid = true;
		double timeElapsed = 0;
		// score starts at 2000*difficulty
		int score = 2000 * (difficulty.ordinal() + 1);
		double fastestTime = calculateFastestTime(maze);

		RegularMazeGameFields fields = new RegularMazeGameFields(maze, player,
				cheese, gameValid, timeElapsed, score, fastestTime, difficulty);
		PlayRegularMaze.play(frame, fields);
	}

	/**
	 * Calculates the fastest time that the maze can be theoretically finished
	 * in
	 * 
	 * @param maze
	 *            the maze for which the fastest time is being calculated
	 * @return the fastest time
	 */
	private static double calculateFastestTime(Maze maze) {
		int shortestPathTileCount = maze.getPath(maze.getTile(1, 1),
				maze.getTile(maze.getNumRows() - 2, maze.getNumCols() - 2))
				.size();
		return shortestPathTileCount * 0.12;
	}
}
