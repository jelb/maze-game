/**This package contains all the classes needed for the generation and running 
 * of the regular maze game mode
 * @author Jeffrey
 *
 */
package regularMaze;