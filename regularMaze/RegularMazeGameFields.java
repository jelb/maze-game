package regularMaze;

import enums.Difficulty;
import gameObjects.Cheese;
import gameObjects.Maze;
import gameObjects.Player;

/**
 * This class holds all the fields needed to run the maze game including the
 * maze, player and cheeze
 * 
 * @author Jeffrey
 * 
 */
public final class RegularMazeGameFields {
	private final Maze maze;
	private final Player player;
	private final Cheese cheese;
	private final boolean gameValid;
	private final double timeElapsed;
	private final int score;
	private final double fastestTime;
	private final Difficulty difficulty;

	/**
	 * Initialises the game fields
	 * 
	 * @param maze
	 *            the maze object
	 * @param player
	 *            the player object
	 * @param cheese
	 *            the cheese object
	 * @param gameValid
	 *            a boolean whether the game is valid
	 * @param timeElapsed
	 *            the amount of time elapsed since the start of the game
	 * @param score
	 *            the players score
	 * @param fastestTime
	 *            the fastest time
	 * @param difficulty
	 *            the difficulty chosen by the player
	 */
	public RegularMazeGameFields(Maze maze, Player player, Cheese cheese,
			boolean gameValid, double timeElapsed, int score,
			double fastestTime, Difficulty difficulty) {
		this.maze = maze;
		this.player = player;
		this.cheese = cheese;
		this.gameValid = gameValid;
		this.timeElapsed = timeElapsed;
		this.score = score;
		this.fastestTime = fastestTime;
		this.difficulty = difficulty;
	}

	/**
	 * Retrieves the fastest time
	 * 
	 * @return the fastest time
	 */
	public double getFastestTime() {
		return fastestTime;
	}

	/**
	 * Retrieves the maze object
	 * 
	 * @return the maze object
	 */
	public Maze getMaze() {
		return maze;
	}

	/**
	 * Retrieves the player object
	 * 
	 * @return the player object
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * Retrieves the cheese object
	 * 
	 * @return the cheese object
	 */
	public Cheese getCheese() {
		return cheese;
	}

	/**
	 * Returns whether the game is valid or not
	 * 
	 * @return the validity of the game
	 */
	public boolean isGameValid() {
		return gameValid;
	}

	/**
	 * Returns the time elapsed since the start of the game
	 * 
	 * @return the time elapsed
	 */
	public double getTimeElapsed() {
		return timeElapsed;
	}

	/**
	 * Returns the players score
	 * 
	 * @return score
	 */
	public int getScore() {
		return score;
	}

	/**
	 * Returns the difficulty of the maze
	 * 
	 * @return difficulty
	 */
	public Difficulty getDifficulty() {
		return difficulty;
	}
}
