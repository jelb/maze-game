package enums;

public enum Direction {
	NORTH, EAST, SOUTH, WEST;
	public Direction clockwise() {
		if (this == NORTH) {
			return EAST;
		}
		if (this == EAST) {
			return SOUTH;
		}
		if (this == SOUTH) {
			return WEST;
		}
		if (this == WEST) {
			return NORTH;
		}
		return null;
	}
}
