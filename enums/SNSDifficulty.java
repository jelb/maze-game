package enums;

public enum SNSDifficulty implements Difficulty {
	EASY(180, 5, 5, 1), MEDIUM(128, 7, 7, 2), HARD(100, 9, 9, 3);

	private int tileWidth;
	private int numRows;
	private int numCols;
	private int startX;
	private int startY;
	private int endX;
	private int endY;
	private int wallThickness;
	private int playerSize;
	private int cheeseSize;
	private int enemySpeed;

	private SNSDifficulty(int tileWidth, int numRows, int numCols,
			int enemySpeed) {
		this.wallThickness = 16;
		this.playerSize = 8;
		this.cheeseSize = 16;
		this.tileWidth = tileWidth;
		this.numRows = numRows;
		this.numCols = numCols;
		this.startX = tileWidth / 2 + playerSize / 2;
		this.startY = tileWidth / 2 + playerSize / 2;
		this.endX = (numCols / 2) * tileWidth + tileWidth / 2;
		this.endY = (numRows / 2) * tileWidth + tileWidth / 2;
		this.enemySpeed = enemySpeed;
	}

	/**
	 * Returns the speed of the enemies
	 * 
	 * @return enemy speed
	 */
	public int getEnemySpeed() {
		return enemySpeed;
	}

	public int getTileWidth() {
		return tileWidth;
	}

	public int getNumRows() {
		return numRows;
	}

	public int getNumCols() {
		return numCols;
	}

	public boolean isAtLeast(RegularMazeDifficulty difficulty) {
		if (this.ordinal() >= difficulty.ordinal()) {
			return true;
		}
		return false;
	}

	@Override
	public int getStartX() {
		return startX;
	}

	@Override
	public int getStartY() {
		return startY;
	}

	@Override
	public int getEndX() {
		return endX;
	}

	@Override
	public int getEndY() {
		return endY;
	}

	@Override
	public int getPlayerSize() {
		return playerSize;
	}

	/**
	 * Returns the thickness of the walls
	 * 
	 * @return wall thickness
	 */
	public int getWallThickness() {
		return wallThickness;
	}

	@Override
	public int getCheeseSize() {
		return cheeseSize;
	}
}
