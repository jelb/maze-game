package enums;

public interface Difficulty {
	/**
	 * Returns the width of the tiles in the maze for this difficulty
	 * 
	 * @return tile width
	 */
	public int getTileWidth();

	/**
	 * Returns the amount of rows there is in the maze
	 * 
	 * @return number of rows
	 */
	public int getNumRows();

	/**
	 * Returns the amount of columns there is in the maze
	 * 
	 * @return number of columns
	 */
	public int getNumCols();

	/**
	 * Returns the x-coordinate of the start of the maze
	 * 
	 * @return x-coordinate
	 */
	public int getStartX();

	/**
	 * Returns the y-coordniate of the start of the maze
	 * 
	 * @return y-coordinate
	 */
	public int getStartY();

	/**
	 * Returns the x-coordinate of the end of the maze
	 * 
	 * @return x-coordinate
	 */
	public int getEndX();

	/**
	 * Retunrs the y-coordinate of the end of the maze
	 * 
	 * @return y-coordinate
	 */
	public int getEndY();

	/**
	 * Checks if this maze difficulty is at least the same as the specified
	 * difficulty
	 * 
	 * @param difficulty
	 * @return true if it is, false if otherwise
	 */
	public boolean isAtLeast(RegularMazeDifficulty difficulty);

	/**
	 * Returns the size of player
	 * 
	 * @return player size
	 */
	public int getPlayerSize();

	/**
	 * Returns the size of the cheese
	 * 
	 * @return cheese size
	 */
	public int getCheeseSize();

	public int ordinal();
}
