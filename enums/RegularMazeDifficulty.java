package enums;

/**This class governs the definition of the difficulty for the regular maze game
 * @author Jeffrey
 *
 */
public enum RegularMazeDifficulty implements Difficulty {
	EASY(60, 15, 15), MEDIUM(36, 25, 25), HARD(29, 31, 31), BRUTAL(20, 45, 45), MASOCHISM(
			12, 75, 75);

	private int tileWidth;
	private int numRows;
	private int numCols;
	private int startX;
	private int startY;
	private int endX;
	private int endY;
	private int playerSize;
	private int cheeseSize;

	private RegularMazeDifficulty(int tileWidth, int numRows, int numCols) {
		this.tileWidth = tileWidth;
		this.numRows = numRows;
		this.numCols = numCols;
		this.startX = tileWidth*5/4;
		this.startY = tileWidth*5/4;
		this.endX = tileWidth*(numCols-2) + tileWidth/4;
		this.endY = tileWidth*(numRows-2) + tileWidth/4;
		this.playerSize = tileWidth/2;
		this.cheeseSize = tileWidth/2;
	}

	public int getTileWidth() {
		return tileWidth;
	}

	public int getNumRows() {
		return numRows;
	}

	public int getNumCols() {
		return numCols;
	}

	public boolean isAtLeast(RegularMazeDifficulty difficulty) {
		if (this.ordinal() >= difficulty.ordinal()) {
			return true;
		}
		return false;
	}

	@Override
	public int getStartX() {
		return startX;
	}
	
	@Override
	public int getStartY() {
		return startY;
	}
	
	@Override
	public int getEndX() {
		return endX;
	}

	@Override
	public int getEndY() {
		return endY;
	}

	@Override
	public int getPlayerSize() {
		return playerSize;
	}

	@Override
	public int getCheeseSize() {
		return cheeseSize;
	}

}
