package gameObjects;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

/**
 * The tiles of the SNS game which the player is safe from enemy objects
 * 
 * @author Jeffrey
 * 
 */
public class SafeTile extends MazeObject {
	private static final long serialVersionUID = 1L;

	/**
	 * Initialises the safeTile
	 * 
	 * @param x
	 *            the x-coordinate of the tile
	 * @param y
	 *            the y-coordinate of the tile
	 * @param width
	 *            the width of the tile
	 * @param height
	 *            the height of the tile
	 */
	public SafeTile(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	/**
	 * @see gameObjects.DrawableObject#draw(java.awt.Graphics2D)
	 */
	@Override
	public void draw(Graphics2D g2) {
		Rectangle2D.Double square = new Rectangle2D.Double(x, y, width - 1,
				height - 1);

		g2.setPaint(new Color(140, 70, 20));
		g2.fill(square);
		g2.draw(square);
	}
}
