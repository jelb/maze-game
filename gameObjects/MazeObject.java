package gameObjects;

import javax.swing.JComponent;

/*
 * x and y represent the top left corner of the object's hitbox. 
 * The hitbox then extends width units down and right.
 * 
 *      <--width-->
 * 		o**********
 * 		***********
 * 		***********
 * 		***********
 * 		***********
 */

/**
 * The Superclass that specifies the common elements of mazeObjects
 * 
 * @author Jeffrey
 * 
 */
public abstract class MazeObject extends JComponent implements DrawableObject {
	private static final long serialVersionUID = 1L;
	protected int x;
	protected int y;
	protected int width;
	protected int height;

	/**
	 * @see javax.swing.JComponent#getX()
	 */
	public int getX() {
		return x;
	}

	/**
	 * Sets the x-coordinate of the object
	 * 
	 * @param x
	 *            the value to set the x-coordinate to
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @see javax.swing.JComponent#getY()
	 */
	public int getY() {
		return y;
	}

	/**
	 * Sets the y-coordinate of the object
	 * 
	 * @param y
	 *            the value to set the y-coordinate to
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * @see javax.swing.JComponent#getWidth()
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Sets the width of the object
	 * 
	 * @param width
	 *            the value to set the width to
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * @see javax.swing.JComponent#getHeight()
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Sets the height of the object
	 * 
	 * @param height
	 *            the value to set the height to
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * Tests whether this item is colliding with another one.
	 * 
	 * @param other
	 *            another MazeObject
	 * @return true if this item is colliding with the other
	 */
	public boolean hitTest(MazeObject other) {
		// There are 4 cases when the hitboxes are NOT colliding

		// top left corner coordinates (x,y) bottom right
		// (x+sideLength,y+sideLength)

		// 1 - obj1 is completely above obj2
		// (bottom edge of obj1 is north of top edge of obj2)
		if (this.y + this.height < other.getY()) {
			return false;
		}
		// 2 - obj1 is completely to the left of obj2
		// (right edge of obj1 is west of left edge of obj2
		if (this.x + this.width < other.getX()) {
			return false;
		}
		// 3 - obj1 is completely below obj2
		// (top edge of obj1 is south of bottom edge of obj2)
		if (this.y > other.getY() + other.getHeight()) {
			return false;
		}
		// 4 - obj1 is completely to the right of obj2
		// (left edge of obj1 is east of right edge of obj2)
		if (this.x > other.getX() + other.getWidth()) {
			return false;
		}
		// if any of these cases are true, then they are not colliding.
		// if none of these cases are true, then they are colliding.

		return true;
	}
}
