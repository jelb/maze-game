package gameObjects;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

import enums.Difficulty;

/**
 * A 2D array containing Tiles. The Tiles are either opened, or closed,
 * indicating whether the Tile is a path, or a wall.
 * 
 * @author Jeffrey
 * 
 */
public class Maze implements DrawableObject {
	private Tile[][] maze;
	private int numRows;
	private int numCols;
	private int tileWidth;

	/**
	 * Initialises a Maze by adding all the Tiles to the entire Area of the
	 * maze. All Tiles are initially considered closed
	 * 
	 * @param difficulty
	 *            this determines the width, length and height of the maze
	 *            depending on how hard it is.
	 */
	public Maze(Difficulty difficulty) {
		int i, j, x, y;
		this.numRows = difficulty.getNumRows();
		this.numCols = difficulty.getNumCols();
		this.tileWidth = difficulty.getTileWidth();
		maze = new Tile[numRows][numCols];
		for (i = 0; i < numRows; i++) {
			for (j = 0; j < numCols; j++) {
				y = i * tileWidth;
				x = j * tileWidth;
				// System.out.println(i + " " + j + " " + x + " " + y);
				maze[i][j] = new Tile(i, j, x, y, tileWidth, tileWidth);
			}
		}
	}

	/**
	 * Initialises a Maze by adding all the Tiles to the entire Area of the
	 * maze. All Tiles are initially considered closed
	 * 
	 * @param numRows
	 *            the number of rows that the maze has
	 * @param numCols
	 *            the number of columns that the maze has
	 * @param tileWidth
	 *            the width/height of each tile within the maze
	 */
	public Maze(int numRows, int numCols, int tileWidth) {
		int i, j, x, y;
		this.numRows = numRows;
		this.numCols = numCols;
		this.tileWidth = tileWidth;
		maze = new Tile[numRows][numCols];
		for (i = 0; i < numRows; i++) {
			for (j = 0; j < numCols; j++) {
				y = i * tileWidth;
				x = j * tileWidth;
				// System.out.println(i + " " + j + " " + x + " " + y);
				maze[i][j] = new Tile(i, j, x, y, tileWidth, tileWidth);
			}
		}
	}

	/**
	 * @see DrawableObject#draw(java.awt.Graphics2D)
	 */
	@Override
	public void draw(Graphics2D g2) {
		for (Tile[] mazeRow : maze) {
			for (Tile t : mazeRow) {
				t.draw(g2);
			}
		}
	}

	/*
	 * T______ | | | P | |______|
	 */

	/**
	 * Generates and shows a hint to the end of the maze from the players
	 * current position
	 * 
	 * @param playerX
	 *            the players x-coordinate
	 * @param playerY
	 *            the players y-coordinate
	 */
	public void showHintPath(int playerX, int playerY) {
		Tile startTile = null;
		for (Tile[] row : maze) {
			for (Tile t : row) {
				/*
				 * T______ | | | P | |______|
				 */
				// if t contains the player position
				// (in horizontal bounds and vertical bounds)
				if (t.getX() <= playerX && t.getX() + t.getWidth() >= playerX
						&& t.getY() <= playerY
						&& t.getY() + t.getHeight() >= playerY) {
					startTile = t;
				}
			}
		}

		Tile endTile = maze[numRows - 2][numCols - 2];

		// rgb values start at highlightColour
		Color highlightColour = new Color(0, 255, 127); // lime green
		int red = highlightColour.getRed();
		int green = highlightColour.getGreen();
		int blue = highlightColour.getBlue();
		ArrayList<Tile> path = getPath(startTile, endTile);
		// each tile is closer to white than the last
		// by increasing the rgb values a step, where there are path.size()
		// steps in total
		for (Tile t : path) {
			t.highlightTile(new Color(red, green, blue));
			red += (255 - highlightColour.getRed()) / path.size();
			green += (255 - highlightColour.getGreen()) / path.size();
			blue += (255 - highlightColour.getBlue()) / path.size();
		}
	}

	/**
	 * Clears the highlighted hint path from the maze
	 */
	public void clearHintPath() {
		for (Tile[] row : maze) {
			for (Tile t : row) {
				if (t.isHighlighted()) {
					t.highlightFade();
				}
			}
		}
	}

	/**
	 * Checks to see that all the tiles that where highlighted for the hint are
	 * now not highlighted anymore
	 * 
	 * @return false if a Tile is still highlighted, true if otherwise.
	 */
	public boolean allTilesUnhighlighted() {
		for (Tile[] row : maze) {
			for (Tile t : row) {
				if (t.isHighlighted()) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Retrieves the number of rows that the maze has
	 * 
	 * @return The number of rows in the maze
	 */
	public int getNumRows() {
		return numRows;
	}

	/**
	 * Retrieves the number of columns that the maze has
	 * 
	 * @return The number of columns in the maze
	 */
	public int getNumCols() {
		return numCols;
	}

	/**
	 * Retrieves the width of the tiles in the maze
	 * 
	 * @return The width of the tiles
	 */
	public int getTileWidth() {
		return tileWidth;
	}

	/**
	 * Retrieves a Tile at a specified coordinate on the maze
	 * 
	 * @param row
	 *            the x-coordinate of the needed Tile
	 * @param col
	 *            the y-coordinate of the needed Tile
	 * @return the Tile at the specified coordinate
	 */
	public Tile getTile(int row, int col) {
		return maze[row][col];
	}

	/**
	 * Opens the Tile so that it can be part of a path
	 * 
	 * @param row
	 *            the x-coordinate of the tile
	 * @param col
	 *            the y-coordinate of the tile
	 */
	public void openTile(int row, int col) {
		if (row >= numRows || col >= numCols) {
			System.out
					.println("something is wrong. row or column out of bounds");
		}
		maze[row][col].setTileOpen();
	}

	/**
	 * Closes the Tile so that it can be a wall within the maze
	 * 
	 * @param row
	 *            the x-coordinate of the maze
	 * @param col
	 *            the y-coordinate of the maze
	 */
	public void closeTile(int row, int col) {
		if (row >= numRows || col >= numCols) {
			System.out
					.println("something is wrong. row or column out of bounds");
		}
		maze[row][col].setTileClosed();
	}

	/**
	 * Checks whether the tile at the specified coordinate is open
	 * 
	 * @param row
	 *            the x-coordinate of the tile
	 * @param col
	 *            the y-coordinate of the tile
	 * @return True if the tile is open, false if otherwise
	 */
	public boolean isOpen(int row, int col) {
		return maze[row][col].isOpen();
	}

	/**
	 * Retrieves an array list of all the closed tiles in the maze
	 * 
	 * @return an arrayList of all the closed tiles
	 */
	public ArrayList<Tile> getClosedTiles() {
		ArrayList<Tile> tiles = new ArrayList<Tile>();
		for (Tile[] row : maze) {
			for (Tile t : row) {
				if (!t.isOpen()) {
					tiles.add(t);
				}
			}
		}
		return tiles;
	}

	/**
	 * Checks whether two Tiles are connected or not. Connected only if tiles
	 * are open and adjacent to each other
	 * 
	 * @param x1
	 *            the x-coordinate of the first Tile
	 * @param y1
	 *            the y-coordinate of the first Tile
	 * @param x2
	 *            the x-coordinate of the second Tile
	 * @param y2
	 *            the y-coordinate of the second Tile
	 * @return True if tiles are connected, false if otherwise
	 */
	public boolean isConnected(int x1, int y1, int x2, int y2) {
		if (!inBounds(x1, y1) || !inBounds(x2, y2)) {
			System.out
					.println("something is wrong. A row or column out of bounds");
			return false;
		}
		if (maze[x1][y1].isOpen() && maze[x2][y2].isOpen()) {
			return isAdjacent(x1, y1, x2, y2);
		}
		return false;
	}

	/**
	 * Checks whether two tiles are directly next to each other
	 * 
	 * @param row1
	 *            the x-coordinate of the first tile
	 * @param col1
	 *            the y-coordinate of the first tile
	 * @param row2
	 *            the x-coordinate of the second tile
	 * @param col2
	 *            the y-coordinate of the second tile
	 * @return true if they are directly adjacent, false if otherwise
	 */
	public boolean isAdjacent(int row1, int col1, int row2, int col2) {
		if (row1 >= numRows || row2 >= numRows || col1 >= numCols
				|| col2 >= numCols) {
			System.out
					.println("something is wrong. A row or column out of bounds");
			return false;
		}
		if (Math.abs(row1 - row2) == 1) {
			if (Math.abs(col1 - col2) == 0) {
				return true;
			}
		} else if (Math.abs(col1 - col2) == 1) {
			if (Math.abs(row1 - row2) == 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns the degree of the tile. i.e. the number of open tiles adjacent to
	 * it.
	 * 
	 * @param tile
	 * @return
	 */
	public int getDegree(Tile tile) {
		int row = tile.getRow();
		int col = tile.getCol();
		int i = 0;

		if (maze[row + 1][col].isOpen()) {
			if (row != numRows - 1) {
				i++;
			}
		}
		if (maze[row - 1][col].isOpen()) {
			if (row != 0) {
				i++;
			}
		}
		if (maze[row][col + 1].isOpen()) {
			if (col != numCols - 1) {
				i++;
			}
		}
		if (maze[row][col - 1].isOpen()) {
			if (col != 0) {
				i++;
			}
		}
		return i;
	}

	/**
	 * Returns an ArrayList containing the shortest path between two *open*
	 * tiles, if the path exists. Returns null if a path was not found.
	 * <p>
	 * This method uses a BFS, expanding to open tiles.
	 * 
	 * @param t1
	 * @param t2
	 * @return An arrayList of all the Tiles to the end of the maze
	 */
	public ArrayList<Tile> getPath(Tile start, Tile dest) {
		Queue<Tile> q = new LinkedList<Tile>();
		q.add(start);

		HashMap<Tile, Tile> parent = new HashMap<Tile, Tile>();
		parent.put(start, start);

		boolean found = false;
		Tile curr = null;

		while (!q.isEmpty() && !found) {
			curr = q.remove(); // remove from queue

			// add neighbours to queue
			for (Tile next : getDirectNeighbours(curr)) {
				if (!next.isOpen()) {
					continue; // ignore if the neighbour tile is closed
				}
				if (!parent.containsKey(next)) {
					parent.put(next, curr);
					q.add(next);
				}
				if (next.equals(dest)) {
					found = true;
					break;
				}
			}
		}

		if (!found) {
			return null;
		}

		ArrayList<Tile> path = new ArrayList<Tile>();
		curr = dest;
		// trace path from end to start
		while (curr != start) {
			path.add(0, curr);
			curr = parent.get(curr);
		}
		path.add(0, curr);
		return path;
	}

	/**
	 * Finds all the tiles which are adjacent to the specified tiles
	 * 
	 * @param t
	 *            the tile to be checked for neighbours
	 * @return an arrayList of all adjacent tiles
	 */
	public ArrayList<Tile> getDirectNeighbours(Tile t) {
		ArrayList<Tile> neighbours = new ArrayList<Tile>();
		int thisRow = t.getRow();
		int thisCol = t.getCol();

		// return an ArrayList containing the tiles adjacent to this tile.
		// that is, 1 away up down left or right
		// be careful of border tiles going out of bounds

		if (inBounds(thisRow + 1, thisCol)) {
			neighbours.add(maze[thisRow + 1][thisCol]);
		}
		if (inBounds(thisRow - 1, thisCol)) {
			neighbours.add(maze[thisRow - 1][thisCol]);
		}
		if (inBounds(thisRow, thisCol + 1)) {
			neighbours.add(maze[thisRow][thisCol + 1]);
		}
		if (inBounds(thisRow, thisCol - 1)) {
			neighbours.add(maze[thisRow][thisCol - 1]);
		}
		return neighbours;
	}

	/**
	 * Finds how many unvisited neighbours there are. Used for maze generation,
	 * where we only care about cell tiles.
	 * 
	 * @param t
	 *            the tile to check for unvisited neighbours
	 * @return how many unvisited neighbours there are
	 */
	public int getCellDegree(Tile t) {
		return getUnvisitedCellNeighbours(t).size();
	}

	/**
	 * Finds all unvisited neighbours there are. Used for maze generation
	 * 
	 * @param t
	 *            the tile to check for unvisited neighbours
	 * @return an arrayList of unvisitedCellNeighbours
	 */
	public ArrayList<Tile> getUnvisitedCellNeighbours(Tile t) {
		int thisRow = t.getRow();
		int thisCol = t.getCol();
		ArrayList<Tile> neighbours = new ArrayList<Tile>();
		if (inBounds(thisRow + 2, thisCol) && !isOpen(thisRow + 2, thisCol)) {
			neighbours.add(maze[thisRow + 2][thisCol]);
		}
		if (inBounds(thisRow - 2, thisCol) && !isOpen(thisRow - 2, thisCol)) {
			neighbours.add(maze[thisRow - 2][thisCol]);
		}
		if (inBounds(thisRow, thisCol + 2) && !isOpen(thisRow, thisCol + 2)) {
			neighbours.add(maze[thisRow][thisCol + 2]);
		}
		if (inBounds(thisRow, thisCol - 2) && !isOpen(thisRow, thisCol - 2)) {
			neighbours.add(maze[thisRow][thisCol - 2]);
		}
		return neighbours;
	}

	/**
	 * Checks if a tile is part of the maze and not a border tile
	 * 
	 * @param row
	 *            the x-coordinate of the tile
	 * @param col
	 *            the y-coordinate of the tile
	 * @return true if the tile is in bounds, false if otherwise
	 */
	private boolean inBounds(int row, int col) {
		if (row <= 0 || col <= 0 || row >= numRows - 1 || col >= numCols - 1) {
			return false;
		}
		return true;
	}

}
