package gameObjects;

import java.awt.Graphics2D;

/*
 * Having this interface is good for when we might want to add extensions to the 
 * maze such as enemy objects, or death-spikes or something like that.
 * 
 * Or actually when we want to display a timer or lives, anything that needs to
 * be drawn.
 * 
 * There will be 2 types of SceneObjects:
 * 
 * 1. maze objects - player, tile. Things that will be in the maze and part of the game
 * 
 * 2. non-maze objects - quit button, help button, hint button, timer, score. 
 * Things that will not be in the maze, and part of the GUI
 */

public interface DrawableObject {
	/**
	 * Draws this item.
	 * 
	 * @param g2
	 *            the graphics context
	 */
	void draw(Graphics2D g2);

}
