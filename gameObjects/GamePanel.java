package gameObjects;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

import javax.swing.JPanel;

/**
 * The panel on which the game will be displayed
 * 
 * @author Jeffrey
 * 
 */
public class GamePanel extends JPanel {
	private static final long serialVersionUID = 1L;
	ArrayList<DrawableObject> gameObjects;

	/**
	 * Initialises the gameObjects field
	 */
	public GamePanel() {
		gameObjects = new ArrayList<DrawableObject>();
	}

	/**
	 * Adds the object into the gameObjects arrayList to be later displayed on
	 * the panel
	 * 
	 * @param object
	 *            the object to be displayed on the GamePanel
	 */
	public synchronized void addObject(DrawableObject object) {
		gameObjects.add(object);
	}

	/**
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	public synchronized void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		for (DrawableObject object : gameObjects) {
			object.draw(g2);
		}
	}
}
