package gameObjects;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import enemyAI.EnemyAI;

/**
 * A class containing additional information needed for the enemies. Extends the
 * Mazeobject class
 * 
 * @author Jeffrey
 * 
 */
public class Enemy extends MazeObject {
	private static final long serialVersionUID = 1L;
	private double dx = 0;
	private double dy = 0;
	private EnemyAI ai;

	/**
	 * Initialises the Enemy
	 * 
	 * @param x
	 *            the x-coordinate on which the enemy will be placed
	 * @param y
	 *            the y-coordinate on which the enemy will be placed
	 * @param size
	 *            the size of the enemy (i.e. width/height)
	 * @param ai
	 *            the type of patrol the enemy will take (horizontal, vertical,
	 *            diagonal etc.)
	 */
	public Enemy(int x, int y, int size, EnemyAI ai) {
		this.x = x;
		this.y = y;
		this.height = size;
		this.width = size;
		this.ai = ai;
		ai.initialiseEnemy(this);
	}

	/**
	 * Sets the change in the movement in the x-plane
	 * 
	 * @param dx
	 *            the change in x
	 */
	public void setDx(double dx) {
		this.dx = dx;
	}

	/**
	 * Sets the change in the movement in the y-plane
	 * 
	 * @param dy
	 *            the change in y
	 */
	public void setDy(double dy) {
		this.dy = dy;
	}

	/**
	 * Moves the enemy to their new x and y positions depending on dx and dy
	 */
	public void moveEnemy() {
		dx = ai.calculateDx(this);
		dy = ai.calculateDy(this);
		x += dx;
		y += dy;
	}

	/**
	 * Retrieves the value of dx
	 * 
	 * @return dx
	 */
	public double getDx() {
		return dx;
	}

	/**
	 * Retrieves the value of dy
	 * 
	 * @return dy
	 */
	public double getDy() {
		return dy;
	}

	/**
	 * @see gameObjects.DrawableObject#draw(java.awt.Graphics2D)
	 */
	@Override
	public void draw(Graphics2D g2) {
		Rectangle2D.Double square = new Rectangle2D.Double(x, y, width, height);
		g2.setPaint(Color.WHITE);
		g2.fill(square);
		g2.setPaint(Color.GRAY);
		g2.draw(square);
	}

}
