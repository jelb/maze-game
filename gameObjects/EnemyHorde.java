package gameObjects;

import java.awt.Graphics2D;
import java.util.ArrayList;

/**
 * This is the class that holds all the enemies and governs which enemy moves
 * next and removes those who get stuck in walls
 * 
 * @author Jeffrey
 * 
 */
public class EnemyHorde implements DrawableObject {
	private ArrayList<Enemy> horde;

	/**
	 * Initialises the enemy horde
	 * 
	 * @param horde
	 *            an arrayList of all the enemies already in the game
	 */
	public EnemyHorde(ArrayList<Enemy> horde) {
		this.horde = horde;
	}

	/** 
	 * @see gameObjects.DrawableObject#draw(java.awt.Graphics2D)
	 */
	@Override
	public void draw(Graphics2D g2) {
		for (Enemy e : horde) {
			e.draw(g2);
		}
	}

	/**
	 * This method goes through all the enemies and calls each of them to make a
	 * move
	 */
	public void move() {
		for (Enemy e : horde) {
			e.moveEnemy();
		}
	}

	/**
	 * Performs a hit test between the enemies and walls. Enemys stuck in walls
	 * are removed from the playing field.
	 * 
	 * @param map
	 *            The map of the maze containing a list of the walls
	 */
	public void cullTheWeak(SNSMap map) {
		for (Tile t : map.getAllWalls()) {
			for (Enemy e : horde) {
				if (e.hitTest(t)) {
					horde.remove(e);
					System.out.println("enemy in wall - culled");
					break;
				}
			}
		}
	}

	/**
	 * Retrieves an arrayList of all the enemies in the game
	 * 
	 * @return
	 */
	public ArrayList<Enemy> getEnemies() {
		return new ArrayList<Enemy>(horde);
	}

}
