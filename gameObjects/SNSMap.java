package gameObjects;

import java.awt.Graphics2D;
import java.util.ArrayList;

/**
 * The maze representation for the Slide Ninja Slide mode
 * 
 * @author Jeffrey
 * 
 */
public class SNSMap implements DrawableObject {
	private ArrayList<Tile> allWalls;
	private ArrayList<SafeTile> safeTiles;

	/**
	 * The initialiser for the map of Slide Ninja slide
	 * 
	 * @param borders
	 *            an ArrayList of tiles on the borders
	 * @param walls
	 *            an ArrayList of tiles for the walls
	 * @param randomBlocks
	 *            an ArrayList of tiles for the floors
	 * @param safeTiles
	 *            an ArrayList of safeTiles
	 */
	public SNSMap(ArrayList<Tile> borders, ArrayList<Tile> walls,
			ArrayList<Tile> randomBlocks, ArrayList<SafeTile> safeTiles) {
		this.allWalls = new ArrayList<Tile>(walls);
		allWalls.addAll(borders);
		allWalls.addAll(randomBlocks);
		this.safeTiles = new ArrayList<SafeTile>(safeTiles);
	}

	/**
	 * Retrieves all the wall tiles
	 * 
	 * @return an ArrayList of all the wall tiles
	 */
	public ArrayList<Tile> getAllWalls() {
		return new ArrayList<Tile>(allWalls);
	}

	/**
	 * Retrieves all the safe tiles on the map
	 * 
	 * @return an ArrayList of all the safeTiles
	 */
	public ArrayList<SafeTile> getAllSafeTiles() {
		return new ArrayList<SafeTile>(safeTiles);
	}

	/**
	 * @see gameObjects.DrawableObject#draw(java.awt.Graphics2D)
	 */
	@Override
	public void draw(Graphics2D g2) {
		// draw safe tiles first so they will be layered below the walls
		for (SafeTile t : safeTiles) {
			t.draw(g2);
		}
		for (Tile t : allWalls) {
			t.draw(g2);
		}
	}

}
