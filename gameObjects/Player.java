package gameObjects;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import enums.Difficulty;

/**
 * This class represents a player as an extension of a MazeObject. It tracks the
 * player's current coordinate position and speed. These are changed by booleans
 * representing which movement keys are activated, acceleration and
 * deceleration, a speed cap, and hit tests with wall tiles in the maze.
 * 
 */
public class Player extends MazeObject {
	private static final long serialVersionUID = 1L;
	private Color playerColour;
	private boolean drawBorder;

	/**
	 * Initialises the Player object
	 * 
	 * @param difficulty
	 *            contains all the data needed to initialise player
	 * @param drawBorder
	 */
	public Player(Difficulty difficulty, boolean drawBorder) {
		this.x = difficulty.getStartX();
		this.y = difficulty.getStartY();
		this.height = difficulty.getPlayerSize();
		this.width = difficulty.getPlayerSize();
		this.playerColour = Color.orange;
		this.drawBorder = drawBorder;
	}

	/**
	 * Initialises the Player object
	 * 
	 * @param x
	 *            the starting x-coordinate for the player
	 * @param y
	 *            the starting y-coordinate for the player
	 * @param height
	 *            the height of the player
	 * @param width
	 *            the width of the player
	 * @param drawBorder
	 */
	public Player(int x, int y, int height, int width, boolean drawBorder) {
		this.x = x;
		this.y = y;
		this.height = height;
		this.width = width;
		this.playerColour = Color.orange;
		this.drawBorder = drawBorder;
	}

	/**
	 * Draws the player. Drawn as an orange rectangle at position (x, y) with
	 * width and height the same value in the fields.
	 */
	@Override
	public void draw(Graphics2D g2) {
		Rectangle2D.Double square = new Rectangle2D.Double(x, y, width, height);
		g2.setPaint(playerColour);
		g2.fill(square);
		if (drawBorder) {
			g2.setPaint(Color.BLACK);
		}
		g2.draw(square);
	}

	/**
	 * @see gameObjects.MazeObject#setX(int)
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @see gameObjects.MazeObject#setY(int)
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * Sets the colour of the object when drawing
	 * 
	 * @param colour
	 */
	public void setColour(Color colour) {
		this.playerColour = colour;
	}

	/**
	 * Gets the colour which the player will be
	 * 
	 * @return colour
	 */
	public Color getColour() {
		return playerColour;
	}

	/**
	 * Returns the draw border
	 */
	public boolean getDrawBorder() {
		return drawBorder;
	}
}
