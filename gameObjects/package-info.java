/**This package contains all the class representations of all the objects
 *  needed for playing the games such as the mazes, players, cheese,
 *   enemies and much more!
 * @author Jeffrey
 *
 */
package gameObjects;