package gameObjects;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

/**
 * A class representation of the Tiles to be used in the mazes
 * 
 * @author Jeffrey
 * 
 */
public class Tile extends MazeObject {

	private static final long serialVersionUID = 1L;
	private final int row;
	private final int col;
	private boolean open;
	private boolean highlighted;
	private Color highlightColour;

	/**
	 * Initialises the tile to be placed in the mazes
	 * 
	 * @param row
	 *            the row in which the tile is placed
	 * @param col
	 *            the column in which the tile is placed
	 * @param x
	 *            the x-coordinate of the tile
	 * @param y
	 *            the y-coordinate of the tile
	 * @param width
	 *            the width of the tile
	 * @param height
	 *            the height of the tile
	 */
	public Tile(int row, int col, int x, int y, int width, int height) {
		this.row = row;
		this.col = col;
		this.x = x;
		this.y = y;
		this.width = width - 1;
		this.height = height - 1;
		this.open = false;
		this.highlighted = false;
		this.highlightColour = new Color(255, 255, 255);
	}

	/**
	 * Used in SNS extension where row and column sometimes don't matter, only
	 * the absolute position of the tile matters.
	 * 
	 * @param x
	 *            the x-coordinate of the tile
	 * @param y
	 *            the y-coordinate of the tile
	 * @param width
	 *            the width of the tile
	 * @param height
	 *            the height of the tile
	 */
	public Tile(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.open = false;
		this.row = 0;
		this.col = 0;
		this.highlighted = false;
		this.highlightColour = new Color(255, 255, 255);
	}

	/**
	 * @see gameObjects.DrawableObject#draw(java.awt.Graphics2D)
	 */
	@Override
	public void draw(Graphics2D g2) {
		Rectangle2D.Double square = new Rectangle2D.Double(x, y, width, height);
		if (!open) {
			g2.setPaint(Color.DARK_GRAY);
		}
		if (highlighted) {
			g2.setPaint(highlightColour);
		}

		if (!open || highlighted) {
			g2.fill(square);
			g2.draw(square);
		}
	}

	/**
	 * Highlights the tile
	 * 
	 * @param c
	 *            the colour the tile will be
	 */
	public void highlightTile(Color c) {
		this.highlighted = true;
		this.highlightColour = c;
	}

	/**
	 * If highlightColour is white, sets highlighted to false. Otherwise,
	 * increments the RGB of highlightColour.
	 */
	public void highlightFade() {
		int red = highlightColour.getRed();
		int green = highlightColour.getGreen();
		int blue = highlightColour.getBlue();
		red++;
		green++;
		blue++;
		if (red > 255) {
			red = 255;
		}
		if (green > 255) {
			green = 255;
		}
		if (blue > 255) {
			blue = 255;
		}
		highlightColour = new Color(red, green, blue);
		if (highlightColour.equals(Color.WHITE)) {
			this.highlighted = false;
		}
	}

	/**
	 * Checks if the tile is highlighted
	 * 
	 * @return true if highlighted, false is otherwise
	 */
	public boolean isHighlighted() {
		return highlighted;
	}

	/**
	 * Retrieves the row of the tile
	 * 
	 * @return row
	 */
	public int getRow() {
		return row;
	}

	/**
	 * Retrieves the column of the tile
	 * 
	 * @return column
	 */
	public int getCol() {
		return col;
	}

	/**
	 * Retrieves the width of the object
	 * 
	 * @return width
	 */
	public int getObjectWidth() {
		return width;
	}

	/**
	 * Checks if tile is open
	 * 
	 * @return true if open, false if otherwise
	 */
	public boolean isOpen() {
		return open;
	}

	/**
	 * Marks this Tile as open. Returns true if the Tile was change from closed
	 * to open, false if the tile was already open.
	 * 
	 * @return true if the Tile was change from closed to open, false if the
	 *         tile was already open
	 */
	public boolean setTileOpen() {
		if (this.open == true) {
			return false;
		}
		this.open = true;
		return true;
	}

	/**
	 * Marks this Tile as closed. Returns true if the Tile was change from open
	 * to closed, false if the tile was already closed.
	 * 
	 * @return true if the Tile was change from open to closed, false if the
	 *         tile was already closed
	 */
	public boolean setTileClosed() {
		if (this.open == false) {
			return false;
		}
		this.open = false;
		return true;
	}

	@Override
	/**
	 * Returns whether or not this Tile is equal to another object. 
	 * First it checks if their references are equal.
	 * Then it checks if the other object is null or a different class.
	 * Finally it checks that the Tiles have the same row and column.
	 * 
	 * @return true if equal, false otherwise
	 */
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || this.getClass() != obj.getClass()) {
			return false;
		}
		Tile other = (Tile) obj;
		return ((this.row == other.getRow() && this.col == other.getCol()));
	}

	@Override
	/**
	 * Returns a hashCode for this Tile. This is calculated through the its
	 * row and column.
	 * 
	 * @return hashCode for this Tile
	 */
	public int hashCode() {
		return (this.row * 19 + this.col * 37);
	}

}
