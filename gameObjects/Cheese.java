package gameObjects;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import enums.Difficulty;

/**
 * This class represents cheese. It is placed at the end of the maze to provide
 * an incentive for the player to complete the maze.
 * 
 * @author Daniel
 * 
 */
public class Cheese extends MazeObject {
	private static final long serialVersionUID = 1L;
	private Color cheeseColour;

	/**
	 * Constructor for an object of class Cheese. It puts the object at the end
	 * of the maze and defines its dimensions
	 * 
	 * @param difficulty
	 */
	public Cheese(Difficulty difficulty) {
		this.x = difficulty.getEndX();
		this.y = difficulty.getEndY();
		this.height = difficulty.getCheeseSize();
		this.width = difficulty.getCheeseSize();
		this.cheeseColour = Color.YELLOW;
	}

	/**
	 * Constructor for an object of class Cheese. It puts the object at the end
	 * of the maze and defines its dimensions
	 * 
	 * @param x
	 *            the x-coordinate on which the cheese will be placed
	 * @param y
	 *            the y-coordinate on which the cheese will be placed
	 * @param height
	 *            the height of the cheese
	 * @param width
	 *            the width of the cheese
	 */
	public Cheese(int x, int y, int height, int width) {
		this.x = x;
		this.y = y;
		this.height = height;
		this.width = width;
	}

	/**
	 * @see DrawableObject#draw(java.awt.Graphics2D)
	 */
	@Override
	public void draw(Graphics2D g2) {
		Rectangle2D.Double square = new Rectangle2D.Double(x, y, width - 1,
				height - 1);
		g2.setPaint(cheeseColour);
		g2.fill(square);
		g2.draw(square);
	}
}
