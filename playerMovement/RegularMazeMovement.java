package playerMovement;

import gameObjects.MazeObject;
import gameObjects.Player;
import gameObjects.Tile;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.HashSet;

/*
 * See graphical documentation/diagrams for better explanations than those
 * given in the comments.
 * 
 * TODO don't forget the Shadow anonymous class
 */
/**
 * This class extends the regular controls up/down/left/right, implements 
 * acceleration and deceleration.
 *
 */
public class RegularMazeMovement extends BasicMovement {
	public RegularMazeMovement(Player player, ArrayList<Tile> wallTiles) {
		super(player, wallTiles);
		// fields from superclass
		setSpeed(2);
		decel = 1;
		accel = 1;
	}
	
	/**
	 * Sets the maximum speed the player can move at. This is calculated as
	 * width/(5-speed).
	 * 
	 * @precondition speed must be in the range [1, 4]
	 * 
	 * @param speed
	 *            the value that speed is changed in accordance to.
	 */
	public void setSpeed(int speed) {
		maxSpeed = player.getWidth() / (5-speed);
	}
	
	/**
	 * Toggles whether "butter" is activated or not. When butter is activated,
	 * deceleration is turned low (scaling to the player's maxSpeed). When 
	 * butter is deactivated, deceleration is 1.
	 */
	public void butter() {
		if (decel == 1) {
			decel = 1*(double)maxSpeed/100;
			System.out.println("Butter activated - decel rate is now " + decel);
		} else {
			decel = 1;
			System.out.println("Butter deactivated - decel rate back to 1");
		}
	}
	
	/**
	 * Calculates the player's speed and acceleration, and alters the player's
	 * position accordingly. Then checks for collisions with wall tiles, and 
	 * makes further adjustments if necessary.
	 */
	@Override
	public void move() {
		calculateSpeed();
		updatePlayerPosition();
		checkCollisions();
	}
	
	private void updatePlayerPosition() {
		player.setX(player.getX() + (int)dx);
		player.setY(player.getY() + (int)dy);
	}

	/*
	 * Checks if the player is colliding with any wall tiles, and alters their
	 * momentum and position accordingly. Collision currently reduces momentum 
	 * by two-thirds.
	 * 
	 * 1. Create a shadow of the player's current position and momentum, and
	 *    loop through every wall tile and check for a hit. The shadow is 
	 *    necessary because a hit means we will be changing the player's 
	 *    position and momentum.
	 * 
	 * 2. Find out the direction that the player hit the tile. Stop the player's 
	 * acceleration in that direction and reset player position to open space
	 * 
	 * 3. Making wallhugging work:
	 * 
	 * ====== TILE1 TILE2 ======
	 *             PLAYER
	 * 
	 * In a situation like above, if the player was moving up-left, they would
	 * register an UP hit with TILE2, and a LEFT hit with TILE1. The LEFT hit
	 * would cause the player to lose horizontal momentum. To prevent this, the
	 * dx and dy values are backed up. If we get a situation like above, there
	 * will be:
	 * - 2 tiles hit in total
	 * - the tiles will have the same row or the same column
	 * Based on that information, we can restore the dx/dy value when needed.
	 * So in the above case, the player would register a LEFT hit with TILE1, 
	 * and lower dx. However, dx would be restored afterwards.
	 */
	private void checkCollisions() {
		int xBackup = player.getX();
		int yBackup = player.getY();
		double dxBackup = this.dx;
		double dyBackup = this.dy;
		int hitCount = 0;
		
		class Shadow extends MazeObject {
			private static final long serialVersionUID = 1L;
			public Shadow (int x, int y, int width, int height) {
				this.x = x;
				this.y = y;
				this.width = width;
				this.height = height;
			}
			@Override
			public void draw(Graphics2D g2) {
			}
		}
		Shadow shadow = new Shadow(player.getX(), player.getY(),
				player.getWidth(), player.getHeight());

		HashSet<Tile> tilesHit = new HashSet<Tile>();
		for (Tile t : wallTiles) { // maybe optimise to local tiles relative to player
			if (shadow.hitTest(t)) {
				tilesHit.add(t);
				// If a side is being hit, the hitboxes of the player and the tile
				// will overlap on that side by maxSpeed units maximum. 
				
				// 1. player moves east into tile
				// right side of player is under maxSpeed units into tile
				if (xBackup + player.getWidth() - t.getX() < maxSpeed) {
					// set player to be left of the tile
					player.setX(t.getX() - player.getWidth() - 1);
					this.dx /= 3;
					hitCount++;
				}
				
				// 3. player moves south into tile
				// bottom side of player is under maxSpeed units into the tile
				if (yBackup + player.getHeight() - t.getY() < maxSpeed) {
					// set player to be above tile
					player.setY(t.getY() - player.getHeight() - 1);
					this.dy /= 3;
					hitCount++;
				}
				
				// 2. player moves west into tile
				// left side of player is under maxSpeed units into the tile
				if (t.getX() + t.getWidth() - xBackup < maxSpeed) {
					// set player to be right of the tile
					player.setX(t.getX() + t.getWidth() + 1);
					this.dx /= 3;
					hitCount++;
				}
				
				// 4. player moves north into tile
				// top side of player is under maxSpeed units into the tile
				if (t.getY() + t.getHeight() - yBackup < maxSpeed) {
					// set player to be under tile
					player.setY(t.getY() + t.getHeight() + 1);
					this.dy /= 3;
					hitCount++;
				}
			}
		}
		ArrayList<Tile> tiles = new ArrayList<Tile>(tilesHit);
		// if only one tile was hit, and it was hit twice, that means the player
		// just ran into a corner. We don't want to lose momentum from hitting
		// corners, so restore dx and dy.
		// also restore x otherwise you can get stuck on corners.
		// this means if you hit a corner perfectly, you will always jump off
		// horizontally
		if (tiles.size() == 1 && hitCount == 2) {
			this.dx = dxBackup;
			player.setX(xBackup);
			this.dy = dyBackup;
		}
		
		// if we are wallhugging, the hit test will say we are hitting one
		// tile vertically and one tile horizontally, resulting in a loss of
		// all momentum. 
		// check if the two tiles hit are in the same row or column, and 
		// restore momentum and x/y position accordingly.
		if (tiles.size() == 2) {
			if (tiles.get(0).getRow() == tiles.get(1).getRow()) {
				dx = dxBackup;
				player.setX(xBackup);
			} else if (tiles.get(0).getCol() == tiles.get(1).getCol()){
				dy = dyBackup;
				player.setY(yBackup);
			}
		}
	}
	
	/*
	 * Calculates the player's speed. Basically, for each direction, check if
	 * the movement key is down. If it is, accelerate in that direction.
	 * Otherwise, decelerate to 0 if necessary. Checks if the player's speed is
	 * above the max speed, and caps it if necessary.
	 * 
	 * It is not always necessary to decelerate. If the player has let go of UP,
	 * they should decelerate if upwards speed > 0. But if upwards speed < 0,
	 * they should not decelerate because this will incorrectly inflate
	 * downwards speed.
	 */
	private void calculateSpeed() {
		// accelerate if keyDown, else decelerate to 0
		if (up) {
			dy -= accel;
		} else {
			if (dy < 0) {
				dy += decel;
			}
		}
		if (down) {
			dy += accel;
		} else {
			if (dy > 0) {
				dy -= decel;
			}
		}
		if (left) {
			dx -= accel;
		} else {
			if (dx < 0) {
				dx += decel;
			}
		}
		if (right) {
			dx += accel;
		} else {
			if (dx > 0) {
				dx -= decel;
			}
		}
		
		// make sure current speed doesn't exceed cap
		if (dx > maxSpeed) {
			dx = maxSpeed;
		}
		if (dx < -maxSpeed) {
			dx = -maxSpeed;
		}
		if (dy > maxSpeed) {
			dy = maxSpeed;
		}
		if (dy < -maxSpeed) {
			dy = -maxSpeed;
		}
	}

}
