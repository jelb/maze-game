package playerMovement;

import gameObjects.Enemy;
import gameObjects.EnemyHorde;
import gameObjects.MazeObject;
import gameObjects.Player;
import gameObjects.SafeTile;
import gameObjects.Tile;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.HashSet;

public class SNSMovement extends BasicMovement {
	private ArrayList<SafeTile> safeTiles;
	private boolean upLock;
	private boolean downLock;
	private boolean leftLock;
	private boolean rightLock;
	private final double iceDecel;
	private final double safeTileDecel;
	private final int baseMaxSpeed;
	private final int thrustMaxSpeed;
	private final int thrustMultiplier;
	private boolean thrust;
	private boolean alive;
	private EnemyHorde horde;
	private Player teamMate;
	private int deathCount;

	public SNSMovement(Player player, ArrayList<Tile> wallTiles,
			ArrayList<SafeTile> safeTiles, EnemyHorde horde, Player teamMate) {
		super(player, wallTiles);
		// fields from super class
		maxSpeed = player.getWidth() / 3;
		this.accel = maxSpeed / (double) 2;
		this.decel = 0;

		this.safeTiles = safeTiles;
		this.horde = horde;
		this.safeTileDecel = 1;
		this.iceDecel = 0;
		this.upLock = false;
		this.downLock = false;
		this.leftLock = false;
		this.rightLock = false;
		this.baseMaxSpeed = maxSpeed;
		this.thrustMultiplier = 2;
		this.thrustMaxSpeed = maxSpeed * thrustMultiplier;
		this.thrust = false;
		this.alive = true;
		this.teamMate = teamMate;
		this.deathCount = 0;
	}

	/**
	 * Calculates the player's speed and acceleration, and alters the player's
	 * position accordingly. Then checks for collisions with wall tiles, and
	 * makes further adjustments if necessary.
	 */
	@Override
	public void move() {
		if (alive) {
			checkEnemies();
			calculateSpeed();
			updatePlayerPosition();
			checkSafeTiles();
			checkCollisions();
		} else {
			checkRescue();
		}
	}

	/**
	 * If this player is tagged by a teammate, they are revived and their speed
	 * and direction are set to that of the player that revived them.
	 */
	private void checkRescue() {
		if (player.hitTest(teamMate)) {
			alive = true;
			playerColourAlive(player);
		}
	}

	private void checkEnemies() {
		for (Enemy e : horde.getEnemies()) {
			if (player.hitTest(e)) {
				alive = false;
				playerColourDead(player);
				deathCount++;
				break;
			}
		}
	}

	/**
	 * Returns the count of deaths
	 * 
	 * @return death count
	 */
	public int getDeathCount() {
		return deathCount;
	}

	private void playerColourAlive(final Player player) {
		player.setColour(player.getColour().brighter());
	}

	private void playerColourDead(Player player) {
		player.setColour(player.getColour().darker());
	}

	/**
	 * When thrust is toggled on, the player gets a speed boost in the current
	 * direction they are moving. This is done by increasing the maxSpeed fields
	 * and multiplying the current dx and dy values by a multiplier.
	 * 
	 * The up, down, left and right booleans are set to false to prevent the
	 * direction from being manipulated after a bounce. (i.e., to maintain the
	 * player's new direction after they bounce. Otherwise they would still
	 * maintain their old direction).
	 * 
	 * They are locked at this speed and direction until thrust is toggled off.
	 * 
	 * The locking is done by setting the toggle field to either true or false,
	 * which affects how other movement methods function.
	 * 
	 * @param toggle
	 */
	public void thrust(boolean toggle) {
		if (toggle == true) {
			if (decel != iceDecel) { // if not on ice
				System.out.println("Can't thrust unless on ice!");
				return;
			}
			maxSpeed = thrustMaxSpeed;
			dx *= thrustMultiplier;
			dy *= thrustMultiplier;
			up = false;
			down = false;
			left = false;
			right = false;
		} else {
			maxSpeed = baseMaxSpeed;
		}
		this.thrust = toggle;
	}

	/**
	 * Sets whether upwards acceleration is increasing or not. This is set to
	 * true if the player is pressing the up key, and false if the player has
	 * released the up key.
	 * 
	 * This has been overridden for Slide Ninja Slide. If thrust is toggled on,
	 * these keys no longer have any effect.
	 * 
	 * @param keyDown
	 *            boolean determining whether to increase upwards acceleration
	 *            or not
	 */
	@Override
	public void up(boolean keyDown) {
		if (!thrust) {
			up = keyDown;
		}
	}

	/**
	 * Sets whether downwards acceleration is increasing or not. This is set to
	 * true if the player is pressing the down key, and false if the player has
	 * released the down key.
	 * 
	 * This has been overridden for Slide Ninja Slide. If thrust is toggled on,
	 * these keys no longer have any effect.
	 * 
	 * @param keyDown
	 *            boolean determining whether to increase downwards acceleration
	 *            or not
	 */
	@Override
	public void down(boolean keyDown) {
		if (!thrust) {
			down = keyDown;
		}
	}

	/**
	 * Sets whether leftwards acceleration is increasing or not. This is set to
	 * true if the player is pressing the left key, and false if the player has
	 * released the left key.
	 * 
	 * This has been overridden for Slide Ninja Slide. If thrust is toggled on,
	 * these keys no longer have any effect.
	 * 
	 * @param keyDown
	 *            boolean determining whether to increase leftwards acceleration
	 *            or not
	 */
	@Override
	public void left(boolean keyDown) {
		if (!thrust) {
			left = keyDown;
		}
	}

	/**
	 * Sets whether rightwards acceleration is increasing or not. This is set to
	 * true if the player is pressing the right key, and false if the player has
	 * released the right key.
	 * 
	 * This has been overridden for Slide Ninja Slide. If thrust is toggled on,
	 * these keys no longer have any effect.
	 * 
	 * @param keyDown
	 *            boolean determining whether to increase rightwards
	 *            acceleration or not
	 */
	@Override
	public void right(boolean keyDown) {
		if (!thrust) {
			right = keyDown;
		}
	}

	private void checkSafeTiles() {
		for (SafeTile t : safeTiles) {
			if (player.hitTest(t)) {
				decel = safeTileDecel;
				return;
			}
		}
		decel = iceDecel;
	}

	private void updatePlayerPosition() {
		player.setX(player.getX() + (int) dx);
		player.setY(player.getY() + (int) dy);
	}

	/*
	 * Checks if the player is colliding with any wall tiles, and alters their
	 * momentum and position accordingly. Collision currently reduces momentum
	 * by two-thirds.
	 * 
	 * 1. Create a shadow of the player's current position and momentum, and
	 * loop through every wall tile and check for a hit. The shadow is necessary
	 * because a hit means we will be changing the player's position and
	 * momentum.
	 * 
	 * 2. Find out the direction that the player hit the tile. Invert the
	 * player's acceleration in that direction and reset player position to open
	 * space
	 * 
	 * 3. Preventing unwanted bounces
	 * 
	 * ====== TILE1 TILE2 ====== PLAYER
	 * 
	 * if the player hits the bottom left of TILE1, it might register a
	 * LEFTbounce instead of an UPbounce, and the player will bounce off in the
	 * wrong direction.
	 * 
	 * To prevent this, back up the dx and dy values and restore if necessary.
	 */
	private void checkCollisions() {
		int xBackup = player.getX();
		int yBackup = player.getY();
		double dxBackup = this.dx;
		double dyBackup = this.dy;

		class Shadow extends MazeObject {
			private static final long serialVersionUID = 1L;

			public Shadow(int x, int y, int width, int height) {
				this.x = x;
				this.y = y;
				this.width = width;
				this.height = height;
			}

			@Override
			public void draw(Graphics2D g2) {
			}
		}
		Shadow shadow = new Shadow(player.getX(), player.getY(),
				player.getWidth(), player.getHeight());

		HashSet<Tile> tilesHit = new HashSet<Tile>();
		int hitCount = 0;
		for (Tile t : wallTiles) { // maybe optimise to local tiles relative to
									// player
			if (shadow.hitTest(t)) {
				tilesHit.add(t);
				// If a side is being hit, the hitboxes of the player and the
				// tile
				// will overlap on that side by maxSpeed units maximum.

				// 1. player moves east into tile
				// right side of player is under maxSpeed units into tile
				if (xBackup + player.getWidth() - t.getX() < maxSpeed) {
					// set player to be left of the tile
					player.setX(t.getX() - player.getWidth() - 1);
					dx = -dx - 1;
					hitCount++;
				}

				// 3. player moves south into tile
				// bottom side of player is under maxSpeed units into the tile
				if (yBackup + player.getHeight() - t.getY() < maxSpeed) {
					// set player to be above tile
					player.setY(t.getY() - player.getHeight() - 1);
					dy = -dy - 1;
					hitCount++;
				}

				// 2. player moves west into tile
				// left side of player is under maxSpeed units into the tile
				if (t.getX() + t.getWidth() - xBackup < maxSpeed) {
					// set player to be right of the tile
					player.setX(t.getX() + t.getWidth() + 1);
					dx = -dx + 1;
					hitCount++;
				}

				// 4. player moves north into tile
				// top side of player is under maxSpeed units into the tile
				if (t.getY() + t.getHeight() - yBackup < maxSpeed) {
					// set player to be under tile
					player.setY(t.getY() + t.getHeight() + 1);
					dy = -dy + 1;
					hitCount++;
				}
			}
		}
		ArrayList<Tile> tiles = new ArrayList<Tile>(tilesHit);

		// if it only one tile twice (i.e. a corner), ignore the bounces
		// and slightly offset x to avoid repeated collisions with that corner
		if (tiles.size() == 1 && hitCount == 2) {
			this.dx = dxBackup;
			this.dy = dyBackup;
			if (decel == iceDecel) { // if we're on ice
				// playerX = xBackup shifted 1 in the direction of dx
				player.setX(xBackup + (int) (dxBackup / Math.abs(dxBackup)));
			}
		}

		// hitting 2 tiles causes a bounce both vertically and horizontally.
		// undo one of these bounces
		if (tiles.size() == 2) {
			// if same row, restore the x and dx, and leave the y bounce
			if (tiles.get(0).getRow() == tiles.get(1).getRow()) {
				dx = dxBackup;
				dy = -dyBackup;
				player.setX(xBackup);
			} else if (tiles.get(0).getCol() == tiles.get(1).getCol()) {
				dy = dyBackup;
				player.setY(yBackup);
				dx = -dxBackup;
			}
		}
		// ignore all bounce calculations on safe tiles
		if (decel > 0) {
			dx = dxBackup;
			dy = dyBackup;
		}
	}

	/*
	 * Calculates the player's speed. Basically, for each direction, check if
	 * the movement key is down. If it is, accelerate in that direction.
	 * Otherwise, decelerate to 0 if necessary. Checks if the player's speed is
	 * above the max speed, and caps it if necessary.
	 * 
	 * For the SNS extension, we want to lock acceleration in a certain to
	 * simulate ice. So if a player starts moving right, they are locked to
	 * moving right temporarily. Locked means they can't move left, and they
	 * will continue accelerating right regardless of whether the right key is
	 * pressed or not.
	 * 
	 * Directions are unlocked once the player's acceleration in a direction has
	 * reached half of max speed.
	 * 
	 * >>>>> The practical purpose of this is to prevent the player from
	 * manually decelerating by accelerating backwards. <<<<<
	 * 
	 * The locking feature is disabled on safe tiles, where deceleration != 0.
	 */
	private void calculateSpeed() {
		if ((up || upLock) && !downLock) {
			dy -= accel;
			upLock = true;
		} else {
			if (dy < 0) {
				dy += decel;
			}
		}

		if ((down || downLock) && !upLock) {
			dy += accel;
			downLock = true;
		} else {
			if (dy > 0) {
				dy -= decel;
			}
		}

		if ((left || leftLock) && !rightLock) {
			dx -= accel;
			leftLock = true;
		} else {
			if (dx < 0) {
				dx += decel;
			}
		}

		if ((right || rightLock) && !leftLock) {
			dx += accel;
			rightLock = true;
		} else {
			if (dx > 0) {
				dx -= decel;
			}
		}

		// make sure current speed doesn't exceed cap
		if (dy < -maxSpeed) { // up
			dy = -maxSpeed;
		}
		if (dy > maxSpeed) { // down
			dy = maxSpeed;
		}
		if (dx < -maxSpeed) { // left
			dx = -maxSpeed;
		}
		if (dx > maxSpeed) { // right
			dx = maxSpeed;
		}

		if (dy < 0) {
			upLock = false;
		}
		if (dy > 0) {
			downLock = false;
		}
		if (dx < 0) {
			leftLock = false;
		}
		if (dx > 0) {
			rightLock = false;
		}

		if (decel != iceDecel) {
			// can't lock when on a safe tile
			upLock = false;
			downLock = false;
			leftLock = false;
			rightLock = false;
			// disable thrust on safe tiles
			thrust(false);
		}
	}

	/**
	 * Returns whether the player is alive or not
	 * 
	 * @return true if alive, false if dead
	 */
	public boolean isPlayerAlive() {
		return alive;
	}
}