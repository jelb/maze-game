package playerMovement;

public interface PlayerMovement {
	public void move();
	public void up(boolean keyDown);
	public void down(boolean keyDown);
	public void left(boolean keyDown);
	public void right(boolean keyDown);
}
