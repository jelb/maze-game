/** This package basically governs the players movements during the game and
 *  updates their positions on the mazes
 * @author Jeffrey
 *
 */
package playerMovement;