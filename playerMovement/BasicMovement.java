package playerMovement;

import java.util.ArrayList;

import gameObjects.Player;
import gameObjects.Tile;

/**
 * This class has the up left down right methods that all PlayerControls will
 * have. It also has the player and the wallTiles required for any hitTesting
 * in derived classes.
 *
 */
public abstract class BasicMovement implements PlayerMovement {
	protected Player player;
	protected boolean up;
	protected boolean down;
	protected boolean left;
	protected boolean right;
	protected double dx;
	protected double dy;
	protected ArrayList<Tile> wallTiles;
	// variables to be initialised by subclass
	protected int maxSpeed = 0;
	protected double decel = 0;
	protected double accel = 0;
	
	public BasicMovement (Player player, ArrayList<Tile> wallTiles) {
		this.player = player;
		this.up = false;
		this.down = false;
		this.left = false;
		this.right = false;
		this.dx = 0;
		this.dy = 0;
		this.wallTiles = wallTiles;
	}
	
	/**
	 * Sets whether upwards acceleration is increasing or not. This is set to
	 * true if the player is pressing the up key, and false if the player has
	 * released the up key.
	 * 
	 * @param keyDown 	boolean determining whether to increase upwards 
	 * 					acceleration or not
	 */
	public void up(boolean keyDown) {
		up = keyDown;
	}
	
	/**
	 * Sets whether to downwards acceleration is increasing or not. This is set to
	 * true if the player is pressing the down key, and false if the player has
	 * released the down key.
	 * 
	 * @param keyDown 	boolean determining whether to increase downwards 
	 * 					acceleration or not
	 */
	public void down(boolean keyDown) {
		down = keyDown;
	}
	
	/**
	 * Sets whether to leftwards acceleration is increasing or not. This is set to
	 * true if the player is pressing the left key, and false if the player has
	 * released the left key.
	 * 
	 * @param keyDown 	boolean determining whether to increase leftwards 
	 * 					acceleration or not
	 */
	public void left(boolean keyDown) {
		left = keyDown;
	}
	
	/**
	 * Sets whether to rightwards acceleration is increasing or not. This is set to
	 * true if the player is pressing the right key, and false if the player has
	 * released the right key.
	 * 
	 * @param keyDown 	boolean determining whether to increase rightwards 
	 * 					acceleration or not
	 */
	public void right(boolean keyDown) {
		right = keyDown;
	}
}
